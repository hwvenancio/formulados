package br.pucpr.pos.game.ai;

import br.pucpr.pos.game.model.Car;
import br.pucpr.pos.game.model.Track;
import br.pucpr.pos.game.util.Pair;

/**
 * Implements opponent cars "intelligence" to follow the track and run at
 * constant speed
 * 
 * @author hwvenancio
 * 
 */
public class CarAI implements AIListener {

	private Track track;
	private Car car;

	public CarAI(Track track, Car car) {
		this.track = track;
		this.car = car;
	}

	public void handle() {
		int y = Math.min(track.getLength() - 1, Math.max(car.getY(), 0));
		Pair<Integer, Integer> segment = track.getData().getSegments().get(y);
		int target = segment.o1 + 8;
		if (car.getX() < target) {
			car.setX(car.getX() + 1);
		} else if (car.getX() > target) {
			car.setX(car.getX() - 1);
		}
		car.setSpeed(5);
	}
}