package br.pucpr.pos.game.ai;

/**
 * AI handlers should implement this interface
 * 
 * @author hwvenancio
 * 
 */
public interface AIListener {

	/**
	 * Executes AI logic
	 */
	public void handle();

}