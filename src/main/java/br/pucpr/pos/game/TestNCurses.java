package br.pucpr.pos.game;

import br.pucpr.pos.game.os.linux.NCurses;

import com.sun.jna.Pointer;

/**
 * https://github.com/ya790206/jncurses/blob/master/src/main/java/ncurses4j/NCurses.java
 * @author herbert
 *
 */
public class TestNCurses {

	public static void main(String[] args) throws InterruptedException {
		Pointer stdscr;

		stdscr = NCurses.initscr();

		if (NCurses.has_colors()) {
			NCurses.start_color();

			if (NCurses.COLOR_PAIRS() > 1) {
				NCurses.init_pair((short) 1, NCurses.COLOR_BLACK, NCurses.COLOR_WHITE);
				NCurses.attron(NCurses.COLOR_PAIR(1));
			}
		}

		NCurses.attron(NCurses.A_BOLD);

		int maxx = NCurses.getmaxx(stdscr);
		int maxy = NCurses.getmaxy(stdscr);

		String message = "Hello World";

		int x = maxx / 2 - message.length() / 2;
		int y = maxy / 2;

		NCurses.erase();
		NCurses.mvprintw(y, x, message);
		NCurses.refresh();

		message = "Hello World 2";

		x = maxx / 2 - message.length() / 2;
		y++;
		NCurses.mvprintw(y, x, message);
		Thread.sleep(2000);
		NCurses.getch();
		NCurses.endwin();
	}
}