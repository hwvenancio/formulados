package br.pucpr.pos.game.os;

public final class OSInfo {

	private OSInfo() {
	}

	public static final String OS_NAME = System.getProperty("os.name");
	/**
     * The prefix String for all Windows OS.
     */
    private static final String OS_NAME_WINDOWS_PREFIX = "Windows";
	public static final boolean IS_OS_WINDOWS = OS_NAME.startsWith(OS_NAME_WINDOWS_PREFIX);

}