package br.pucpr.pos.game.os;

import br.pucpr.pos.game.util.Sprite;

public interface Screen {

	void initialize();

	void terminate();

	void draw(Sprite s);
}