package br.pucpr.pos.game.os.win;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Structure;
import com.sun.jna.Union;

@SuppressWarnings("rawtypes")
public final class Kernel32 {

	private Kernel32() {
	}

	// typedef struct _CHAR_INFO {
	// union {
	// WCHAR UnicodeChar;
	// CHAR AsciiChar;
	// } Char;
	// WORD Attributes;
	// } CHAR_INFO, *PCHAR_INFO;
	public static class CHAR_INFO extends Structure {
		public CHAR_INFO() {
		}

		public CHAR_INFO(char c, short attr) {
			uChar = new UnionChar(c);
			Attributes = attr;
		}

		public CHAR_INFO(byte c, short attr) {
			uChar = new UnionChar(c);
			Attributes = attr;
		}

		public UnionChar uChar;
		public short Attributes;

		@Override
		protected List getFieldOrder() {
			return Arrays.asList("uChar", "Attributes");
		}

		public static CHAR_INFO[] createArray(int size) {
			return (CHAR_INFO[]) new CHAR_INFO().toArray(size);
		}
	}

	// typedef struct _CONSOLE_CURSOR_INFO {
	// DWORD dwSize;
	// BOOL bVisible;
	// } CONSOLE_CURSOR_INFO, *PCONSOLE_CURSOR_INFO;
	public static class CONSOLE_CURSOR_INFO extends Structure {
		public int dwSize;
		public boolean bVisible;

		@Override
		protected List getFieldOrder() {
			return Arrays.asList("dwSize", "bVisible");
		}

		public static class ByReference extends CONSOLE_CURSOR_INFO implements Structure.ByReference {
		}
	}

	// typedef struct _CONSOLE_SCREEN_BUFFER_INFO {
	// COORD dwSize;
	// COORD dwCursorPosition;
	// WORD wAttributes;
	// SMALL_RECT srWindow;
	// COORD dwMaximumWindowSize;
	// } CONSOLE_SCREEN_BUFFER_INFO;
	public static class CONSOLE_SCREEN_BUFFER_INFO extends Structure {
		public COORD dwSize;
		public COORD dwCursorPosition;
		public short wAttributes;
		public SMALL_RECT srWindow;
		public COORD dwMaximumWindowSize;

		@Override
		protected List getFieldOrder() {
			return Arrays.asList("dwSize", "dwCursorPosition", "wAttributes", "srWindow", "dwMaximumWindowSize");
		}
	}

	// typedef struct _COORD {
	// SHORT X;
	// SHORT Y;
	// } COORD, *PCOORD;
	public static class COORD extends Structure implements Structure.ByValue {
		public COORD() {
		}

		public COORD(short X, short Y) {
			this.X = X;
			this.Y = Y;
		}

		public short X;
		public short Y;

		@Override
		protected List getFieldOrder() {
			return Arrays.asList("X", "Y");
		}
	}

	// typedef struct _INPUT_RECORD {
	// WORD EventType;
	// union {
	// KEY_EVENT_RECORD KeyEvent;
	// MOUSE_EVENT_RECORD MouseEvent;
	// WINDOW_BUFFER_SIZE_RECORD WindowBufferSizeEvent;
	// MENU_EVENT_RECORD MenuEvent;
	// FOCUS_EVENT_RECORD FocusEvent;
	// } Event;
	// } INPUT_RECORD;
	public static class INPUT_RECORD extends Structure {
		public static final short FOCUS_EVENT = 0x0010;
		public static final short KEY_EVENT = 0x0001;
		public static final short MENU_EVENT = 0x0008;
		public static final short MOUSE_EVENT = 0x0002;
		public static final short WINDOW_BUFFER_SIZE_EVENT = 0x0004;

		public short EventType;
		public EventUnion Event;

		@Override
		protected List getFieldOrder() {
			return Arrays.asList("EventType", "Event");
		}

		public static class EventUnion extends Union {
			public KEY_EVENT_RECORD KeyEvent;
			public MOUSE_EVENT_RECORD MouseEvent;
			// WINDOW_BUFFER_SIZE_RECORD WindowBufferSizeEvent;
			// MENU_EVENT_RECORD MenuEvent;
			// FOCUS_EVENT_RECORD FocusEvent;
		}

		@Override
		public void read() {
			readField("EventType");
			switch (EventType) {
			case KEY_EVENT:
				Event.setType(KEY_EVENT_RECORD.class);
				break;
			case MOUSE_EVENT:
				Event.setType(MOUSE_EVENT_RECORD.class);
				break;
			}
			super.read();
		}
	}

	// typedef struct _KEY_EVENT_RECORD {
	// BOOL bKeyDown;
	// WORD wRepeatCount;
	// WORD wVirtualKeyCode;
	// WORD wVirtualScanCode;
	// union {
	// WCHAR UnicodeChar;
	// CHAR AsciiChar;
	// } uChar;
	// DWORD dwControlKeyState;
	// } KEY_EVENT_RECORD;
	public static class KEY_EVENT_RECORD extends Structure {
		public boolean bKeyDown;
		public short wRepeatCount;
		public short wVirtualKeyCode;
		public short wVirtualScanCode;
		public UnionChar uChar;
		public int dwControlKeyState;
		
		@Override
		protected List getFieldOrder() {
			return Arrays.asList("bKeyDown", "wRepeatCount", "wVirtualKeyCode", "wVirtualScanCode", "uChar", "dwControlKeyState");
		}
	}

	// typedef struct _MOUSE_EVENT_RECORD {
	// COORD dwMousePosition;
	// DWORD dwButtonState;
	// DWORD dwControlKeyState;
	// DWORD dwEventFlags;
	// } MOUSE_EVENT_RECORD;
	public static class MOUSE_EVENT_RECORD extends Structure {
		public static final short MOUSE_MOVED = 0x0001;
		public static final short DOUBLE_CLICK = 0x0002;
		public static final short MOUSE_WHEELED = 0x0004;
		public static final short MOUSE_HWHEELED = 0x0008;

		public static final short FROM_LEFT_1ST_BUTTON_PRESSED = 0x0001;
		public static final short RIGHTMOST_BUTTON_PRESSED = 0x0002;
		public static final short FROM_LEFT_2ND_BUTTON_PRESSED = 0x0004;
		public static final short FROM_LEFT_3RD_BUTTON_PRESSED = 0x0008;
		public static final short FROM_LEFT_4TH_BUTTON_PRESSED = 0x0010;

		public COORD dwMousePosition;
		public int dwButtonState;
		public int dwControlKeyState;
		public int dwEventFlags;

		@Override
		protected List getFieldOrder() {
			return Arrays.asList("dwMousePosition", "dwButtonState", "dwControlKeyState", "dwEventFlags");
		}
	}

	// typedef struct _SMALL_RECT {
	// SHORT Left;
	// SHORT Top;
	// SHORT Right;
	// SHORT Bottom;
	// } SMALL_RECT;
	public static class SMALL_RECT extends Structure {
		public SMALL_RECT() {
		}

		public SMALL_RECT(short Top, short Left, short Bottom, short Right) {
			this.Top = Top;
			this.Left = Left;
			this.Bottom = Bottom;
			this.Right = Right;
		}

		public short Left;
		public short Top;
		public short Right;
		public short Bottom;

		@Override
		protected List getFieldOrder() {
			return Arrays.asList("Top", "Left", "Bottom", "Right");
		}
	}

	public static class UnionChar extends Union {
		public UnionChar() {
		}

		public UnionChar(char c) {
			setType(char.class);
			UnicodeChar = c;
		}

		public UnionChar(byte c) {
			setType(byte.class);
			AsciiChar = c;
		}

		public void set(char c) {
			setType(char.class);
			UnicodeChar = c;
		}

		public void set(byte c) {
			setType(byte.class);
			AsciiChar = c;
		}

		public char UnicodeChar;
		public byte AsciiChar;
	}
}