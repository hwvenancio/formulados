package br.pucpr.pos.game.os;

import java.nio.charset.Charset;

import br.pucpr.pos.game.os.win.FileAPI;
import br.pucpr.pos.game.os.win.Kernel32.CHAR_INFO;
import br.pucpr.pos.game.os.win.Kernel32.CONSOLE_SCREEN_BUFFER_INFO;
import br.pucpr.pos.game.os.win.Kernel32.COORD;
import br.pucpr.pos.game.os.win.Kernel32.SMALL_RECT;
import br.pucpr.pos.game.os.win.WinCon;
import br.pucpr.pos.game.util.Sprite;

import com.sun.jna.Pointer;
import java.nio.CharBuffer;

public class WindowsScreen implements Screen {

	private Pointer hOutput;
	private final CONSOLE_SCREEN_BUFFER_INFO consoleInfo = new CONSOLE_SCREEN_BUFFER_INFO();
	CHAR_INFO[] buffer = CHAR_INFO.createArray(25 * 80);

	@Override
	public void initialize() {
		int w = 80;
		int h = 25;
		if (WinCon.INSTANCE.GetConsoleWindow() == null)
			WinCon.INSTANCE.AllocConsole();
		hOutput = FileAPI.INSTANCE.CreateFile("CONOUT$", FileAPI.GENERIC_READ | FileAPI.GENERIC_WRITE, FileAPI.FILE_SHARE_WRITE,
				Pointer.NULL, FileAPI.OPEN_EXISTING, 0, Pointer.NULL);

		WinCon.INSTANCE.GetConsoleScreenBufferInfo(hOutput, consoleInfo);

		WinCon.INSTANCE.SetConsoleWindowInfo(hOutput, true, new SMALL_RECT((short) 0, (short) 0, (short) 1, (short) 1));
		WinCon.INSTANCE.SetConsoleScreenBufferSize(hOutput, new COORD((short) w, (short) h));
		WinCon.INSTANCE.SetConsoleWindowInfo(hOutput, true, new SMALL_RECT((short) 0, (short) 0, (short) (h - 1), (short) (w - 1)));
	}

	@Override
	public void draw(Sprite s) {
		COORD dwBufferSize = new COORD();
		dwBufferSize.X = 80;
		dwBufferSize.Y = 25;
		COORD dwBufferCoord = new COORD();
		dwBufferCoord.X = 0;
		dwBufferCoord.Y = 0;
		SMALL_RECT rcRegion = new SMALL_RECT();
		rcRegion.Left = 0;
		rcRegion.Top = 0;
		rcRegion.Right = 79;
		rcRegion.Bottom = 24;

		int[][] pixels = s.getPixels();
		int width = s.getWidth();
		int height = s.getHeight();
		Charset charSet = Charset.forName("Cp437");
		CharBuffer charBuffer = CharBuffer.wrap(new char[1]);
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				int pixel = pixels[j][i];
				int bg = convertAnsiToDosColor(Sprite.getBG(pixel));
				int fg = convertAnsiToDosColor(Sprite.getFG(pixel));
				int code = Sprite.getCode(pixel);
				charBuffer.put(0, (char) code);
				charBuffer.rewind();
				buffer[j * width + i].uChar.set((char)code);
				buffer[j * width + i].Attributes = (short) (bg << 4 | fg);
			}
		}

		WinCon.INSTANCE.WriteConsoleOutput(hOutput, buffer, dwBufferSize, dwBufferCoord, rcRegion);
	}

	private static int convertAnsiToDosColor(int ansiColor) {
		// Id Ansi Dos
		// 0 - Black ---- Black
		// 1 - Red ------ Blue
		// 2 - Green ---- Green
		// 3 - Yellow --- Aqua
		// 4 - Blue ----- Red
		// 5 - Magenta -- Purple
		// 6 - Cyan ----- Yellow
		// 7 - White ---- White
		switch (ansiColor) {
		case 1:
			return 4;
		case 3:
			return 6;
		case 4:
			return 1;
		case 6:
			return 3;
		default:
			return ansiColor;
		}
	}

	@Override
	public void terminate() {
	}
}