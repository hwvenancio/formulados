package br.pucpr.pos.game.os;


public final class ScreenProvider {
	private ScreenProvider() {
	}

	public static Screen getScreen() {
		if(OSInfo.IS_OS_WINDOWS) {
			return new WindowsScreen();
		} else {
			return new LinuxScreen();
		}
	}
}