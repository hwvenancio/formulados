package br.pucpr.pos.game.os;

import static br.pucpr.pos.game.os.linux.NCurses.*;
import br.pucpr.pos.game.os.linux.NCurses;
import br.pucpr.pos.game.util.Sprite;


public class LinuxScreen implements Screen {

	private short[] colors;

	@Override
	public void initialize() {
		NCurses.initscr();

		if (NCurses.has_colors()) {
			NCurses.start_color();

			if (NCurses.COLOR_PAIRS() > 1) {
				colors = new short[] {COLOR_BLACK, COLOR_RED, COLOR_GREEN, COLOR_YELLOW, COLOR_BLUE, COLOR_MAGENTA, COLOR_CYAN, COLOR_WHITE};
				
				short pairId = 0;
				for(short bg : colors) {
					for(short fg : colors) {
						pairId++;
						NCurses.init_pair(pairId, fg, bg);
					}
				}
			}
		}
	}

	@Override
	public void draw(Sprite s) {
		int width = s.getWidth();
		int height = s.getHeight();
		
		for(int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				int bg = s.getBG(i, j);
				int fg = s.getFG(i, j);
				int code = s.getCode(i, j);
				int pairId = bg*colors.length + fg + 1;
				NCurses.attron(NCurses.COLOR_PAIR(pairId));
				NCurses.mvprintw(j, i, new String(new char[]{(char)code}));
			}
		}
		NCurses.refresh();
	}

	@Override
	public void terminate() {
		NCurses.getch();
		NCurses.endwin();
	}
}