package br.pucpr.pos.game.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class Sprite {

	public static final int BLACK = 0;
	public static final int RED = 1;
	public static final int GREEN = 2;
	public static final int YELLOW = 3; 
	public static final int BLUE = 4; 
	public static final int MAGENTA = 5; 
	public static final int CYAN = 6; 
	public static final int WHITE = 7;
	public static final int DEFAULT = 9;

	private int[][] pixels;

	public Sprite(int width, int height) {
		pixels = new int[height][width];
	}

	public int getCode(int x, int y) {
		return getCode(pixels[y][x]);
	}

	public void setCode(int x, int y, int code) {
		pixels[y][x] = setCode(pixels[y][x], code);
	}

	public int getFG(int x, int y) {
		return getFG(pixels[y][x]);
	}

	public void setFG(int x, int y, int color) {
		pixels[y][x] = setFG(pixels[y][x], color);
	}

	public int getBG(int x, int y) {
		return getBG(pixels[y][x]);
	}

	public void setBG(int x, int y, int color) {
		pixels[y][x] = setBG(pixels[y][x], color);
	}

	public static int getCode(int pixel) {
		return (pixel & 0x0000FFFF);
	}
	public static int getFG(int pixel) {
		return (pixel & 0x00FF0000) >> 16;
	}
	public static int getBG(int pixel) {
		return (pixel & 0xFF000000) >> 24;
	}
	public static int setCode(int pixel, int code) {
		return (pixel & 0xFFFF0000) | ((code & 0x0000FFFF));
	}
	public static int setFG(int pixel, int color) {
		return (pixel & 0xFF00FFFF) | ((color & 0x000000FF) << 16);
	}
	public static int setBG(int pixel, int color) {
		return (pixel & 0x00FFFFFF) | ((color & 0x000000FF) << 24);
	}

	public int[][] getPixels() {
		return pixels;
	}

	public int getWidth() {
		return pixels.length > 0 ? pixels[0].length : 0;
	}

	public int getHeight() {
		return pixels.length;
	}

	public void draw(int x, int y, Sprite s) {
		int minX = Math.max(x, 0);
		int maxX = Math.min(x + s.getWidth(), getWidth());
		int minY = Math.max(y, 0);
		int maxY = Math.min(y + s.getHeight(), getHeight());
		for(int j = minY; j < maxY; j++) {
			for(int i = minX; i < maxX; i++) {
				try {
					int p = s.getPixels()[j - y][i - x];
					if(getBG(p) == DEFAULT) {
						p = setBG(p, getBG(pixels[j][i]));
					}
					pixels[j][i] = p;
				} catch (ArrayIndexOutOfBoundsException e) {
					System.out.println("i: " + i + " j: " + j);
					throw e;
				}
			}
		}
	}

	public static Sprite fromUrl(URL resource) {
		List<String> lines = new LinkedList<String>();
		int width = 0;
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(resource.openStream()));
			try {
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					lines.add(inputLine);
					width = Math.max(width, inputLine.length() / 8);
				}
			} finally {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		int height = lines.size();
		Sprite s = new Sprite(width, height);
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				if(x*8 + 8 > lines.get(y).length())
					break;
				s.pixels[y][x] = Integer.parseInt(lines.get(y).substring(x*8, x*8+8), 16);
			}
		}
		return s;
	}
}