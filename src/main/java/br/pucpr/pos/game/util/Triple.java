package br.pucpr.pos.game.util;

public class Triple<T1, T2, T3> extends Pair<T1, T2> {

	public final T3 o3;

	public Triple(T1 o1, T2 o2, T3 o3) {
		super(o1, o2);
		this.o3 = o3;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((o3 == null) ? 0 : o3.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Triple other = (Triple) obj;
		if (o3 == null) {
			if (other.o3 != null)
				return false;
		} else if (!o3.equals(other.o3))
			return false;
		return true;
	}
}