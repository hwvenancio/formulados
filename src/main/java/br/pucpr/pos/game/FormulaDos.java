package br.pucpr.pos.game;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import br.pucpr.pos.game.core.MenuSceneFactory;
import br.pucpr.pos.game.core.RaceSceneFactory;
import br.pucpr.pos.game.core.Scene;
import br.pucpr.pos.game.core.SceneLifecycle;
import br.pucpr.pos.game.core.SceneManager;
import br.pucpr.pos.game.core.SceneName;
import br.pucpr.pos.game.core.WinSceneFactory;
import br.pucpr.pos.game.os.Screen;
import br.pucpr.pos.game.os.ScreenProvider;

/**
 * This program uses some libraries with native access in order to run
 * without problems in different operational systems.
 * 
 * Jansi: implement ansi escaped control characters for linux terminal
 * and windows prompt
 * - Site: https://github.com/fusesource/jansi
 * 
 * JNativeHook: listens to native events from keyboard (and mouse), otherwise
 * only key strokes are detected, and that depends on system configuration of
 * auto-repetition delay and rate.
 * - Site: https://code.google.com/p/jnativehook/
 * 
 * ---
 * 
 * Documentation about ansi escaped control characters and unicode escaped
 * characters: http://ascii-table.com/ansi-escape-sequences-vt-100.php
 * http://ascii-table.com/ascii-extended-pc-list.php
 * 
 * to debug, run:
 *     java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=127.0.0.1:9999,suspend=y -jar target/FormulaDos-0.0.1-SNAPSHOT-jar-with-dependencies.jar
 * 
 * Then connect with eclipse remote debugger to localhost:9999
 */
public class FormulaDos {


	public static void main(String[] args) throws InterruptedException {
		new FormulaDos().run();
	}

	private Screen screen;
	private boolean eventLoopHooked;
	private SceneManager sceneManager;

	protected void initialize() {
		// initialize native elements
		try {
			Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
			logger.setLevel(Level.OFF);
			GlobalScreen.registerNativeHook();
			eventLoopHooked = true;
		} catch (NativeHookException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		screen = ScreenProvider.getScreen();
		screen.initialize();
		
		sceneManager = new SceneManager();
		sceneManager.addFactory(SceneName.MENU, new MenuSceneFactory());
		sceneManager.addFactory(SceneName.RACE, new RaceSceneFactory());
		sceneManager.addFactory(SceneName.WIN, new WinSceneFactory());
		sceneManager.setFirstScene(SceneName.MENU);
	}

	protected void terminate() {
		if(eventLoopHooked)
			try {
				GlobalScreen.unregisterNativeHook();
			} catch (NativeHookException e) {
				e.printStackTrace();
			}
		if(screen != null)
			screen.terminate();
		try {
			System.out.println("Input buffer: " + System.in.available());
			while(System.in.available() > 0)
				System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() throws InterruptedException {
		initialize();

		try {
			Scene currentScene = null;
			while(sceneManager.hasNext(currentScene)) {
				SceneLifecycle sceneLifecycle = sceneManager.next(currentScene);
				currentScene = new Scene();
				currentScene.setScreen(screen);
				try {
					sceneLifecycle.initialize(currentScene);
					currentScene.run();
				} finally {
					sceneLifecycle.terminate(currentScene);
				}
			}
		} finally {
			terminate();
		}
	}
}