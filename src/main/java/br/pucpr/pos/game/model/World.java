package br.pucpr.pos.game.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import br.pucpr.pos.game.util.Triple;

/**
 * Holds all models relevant to a scene
 * 
 * @author hwvenancio
 *
 */
public class World extends Model {

	private Map<Integer, Set<Model>> layers = new TreeMap<Integer, Set<Model>>();
	private Map<Model, Lifecycle<Model>> lifecycles = new HashMap<Model, Lifecycle<Model>>();
	private List<Triple<Integer, Model, Lifecycle<Model>>> addList = new LinkedList<Triple<Integer, Model, Lifecycle<Model>>>();
	private List<Model> removeList = new LinkedList<Model>();

	public Map<Integer, Set<Model>> getLayers() {
		return layers;
	}

	@SuppressWarnings("unchecked")
	public <T extends Model> T addModel(int layer, T model) {
		return addModel(layer, model, (Lifecycle<T>)Lifecycle.NULL_LIFECYCLE);
	}

	@SuppressWarnings("unchecked")
	public <T extends Model> T addModel(int layer, T model, Lifecycle<T> lifecycle) {
		lifecycles.put(model, (Lifecycle<Model>) lifecycle);
		if (!layers.containsKey(layer)) {
			layers.put(layer, new LinkedHashSet<Model>());
		}
		layers.get(layer).add(model);
		model.setWorld(this);
		lifecycle.initialize(model);
		return model;
	}

	public void removeModel(Model model) {
		Iterator<Entry<Integer, Set<Model>>> it = layers.entrySet().iterator();
		while(it.hasNext()) {
			Entry<Integer, Set<Model>> layer = it.next();
			Set<Model> l = layer.getValue();
			if(l.contains(model)) {
				l.remove(model);
				Lifecycle<Model> lc = lifecycles.remove(model);
				if(lc != null)
					lc.terminate(model);
			}
			if(l.size() == 0)
				it.remove();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void queueAddition(int layer, Model model, Lifecycle lifecycle) {
		addList.add(new Triple<Integer, Model, Lifecycle<Model>>(layer, model, lifecycle));
	}

	public void queueRemoval(Model model) {
		removeList.add(model);
	}

	public List<Triple<Integer,Model,Lifecycle<Model>>> getAdditionQueue() {
		return addList;
	}

	public List<Model> getRemovalQueue() {
		return removeList;
	}
}