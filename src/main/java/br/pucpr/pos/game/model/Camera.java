package br.pucpr.pos.game.model;

import br.pucpr.pos.game.util.Box;

/**
 * Represents a camera state (position). Should use the updater to change it's
 * position in order to track an object
 * 
 * @author hwvenancio
 * 
 */
public class Camera extends Model {

	private int minX;
	private int maxX;
	private int minY;
	private int maxY;

	public Camera() {
		minX = Integer.MIN_VALUE;
		maxX = Integer.MAX_VALUE;
		minY = Integer.MIN_VALUE;
		maxY = Integer.MAX_VALUE;
		setCollisionBox(new Box(0, 0, 80, 25));
	}

	public Box getViewPort() {
		return getTranslatedCollisionBox();
	}

	public int getMinX() {
		return minX;
	}

	public void setMinX(int minX) {
		this.minX = minX;
	}

	public int getMaxX() {
		return maxX;
	}

	public void setMaxX(int maxX) {
		this.maxX = maxX;
	}

	public int getMinY() {
		return minY;
	}

	public void setMinY(int minY) {
		this.minY = minY;
	}

	public int getMaxY() {
		return maxY;
	}

	public void setMaxY(int maxY) {
		this.maxY = maxY;
	}

	@Override
	public void setX(int x) {
		super.setX(Math.max(minX, Math.min(maxX, x)));
	}

	@Override
	public void setY(int y) {
		super.setY(Math.max(minY, Math.min(maxY, y)));
	}
}