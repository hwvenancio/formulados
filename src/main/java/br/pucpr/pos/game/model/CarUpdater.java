package br.pucpr.pos.game.model;

/**
 * Changes car position based on speed, and updates speed based on friction
 * 
 * @author hwvenancio
 * 
 */
public class CarUpdater implements Updater<Car> {

	private int timeStep;

	public CarUpdater(int ticksPS) {
		timeStep = ticksPS;
	}

	@Override
	public void update(Car car) {
		int speed = car.getSpeed(); // pixels per seconds (ticks)
		if (speed != 0) {
			int inverseSpeed = 1000 / Math.abs(speed); // milliseconds per pixels
			if (timeStep * car.incrementUpdateCount() >= inverseSpeed) {
				car.resetUpdateCount();
				if (speed > 0) {
					car.setY(car.getY() - 1);
					car.setSpeed(car.getSpeed() - car.getFriction());
				} else {
					car.setY(car.getY() + 1);
					car.setSpeed(car.getSpeed() + car.getFriction());
				}
			}
		} else {
			car.resetUpdateCount();
		}
	}

}
