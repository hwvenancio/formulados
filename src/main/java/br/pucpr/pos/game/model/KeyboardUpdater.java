package br.pucpr.pos.game.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import br.pucpr.pos.game.core.KeyListener;

/**
 * Bridges native event loop to game loop. Call all registered
 * {@link KeyListener} handle() method when a key is pressed
 * 
 * @author hwvenancio
 * 
 */
public class KeyboardUpdater implements Updater<Model>, NativeKeyListener {

	private NativeKeyEvent lastEvent;
	private Set<Integer> pressedKeys;

	private int scanSpeed;
	private int timeStep;

	// registers key listeners
	private Map<Integer, Set<KeyListener>> listeners;

	// used in the update loop
	private Map<KeyListener, List<Integer>> map;

	public KeyboardUpdater(int ticksPS) {
		// Native Listeners
		lastEvent = new NativeKeyEvent(0, 0, 0, 0, 0, NativeKeyEvent.CHAR_UNDEFINED);
		pressedKeys = new HashSet<Integer>();
		// Game loop
		scanSpeed = 100;
		timeStep = ticksPS;
		listeners = new HashMap<Integer, Set<KeyListener>>();
		map = new HashMap<KeyListener, List<Integer>>();
	}

	public NativeKeyEvent getLastEvent() {
		return lastEvent;
	}

	public int[] getPressedKeys() {
		synchronized (pressedKeys) {
			int[] buffer = new int[pressedKeys.size()];
			Iterator<Integer> it = pressedKeys.iterator();
			for (int index = 0; index < buffer.length; index++) {
				buffer[index] = it.next();
			}
			return buffer;
		}
	}

	@Override
	public void nativeKeyPressed(NativeKeyEvent e) {
		synchronized (pressedKeys) {
			lastEvent = e;
			pressedKeys.add(e.getKeyCode());
		}
	}

	@Override
	public void nativeKeyReleased(NativeKeyEvent e) {
		synchronized (pressedKeys) {
			lastEvent = e;
			pressedKeys.remove(e.getKeyCode());
		}
	}

	@Override
	public void nativeKeyTyped(NativeKeyEvent e) {
	}

	public void addKeyListener(int keyCode, KeyListener l) {
		if (!listeners.containsKey(keyCode))
			listeners.put(keyCode, new LinkedHashSet<KeyListener>());
		listeners.get(keyCode).add(l);
	}

	public void removeKeyListener(int keyCode, KeyListener l) {
		if (listeners.containsKey(keyCode)) {
			Set<KeyListener> set = listeners.get(keyCode);
			set.remove(l);
			if (set.size() == 0)
				listeners.put(keyCode, null);
		}
	}

	@Override
	public void update(Model model) {
		if (timeStep * model.incrementUpdateCount() >= scanSpeed) {
			model.resetUpdateCount();
			map.clear();
			for (int key : getPressedKeys()) {
				if (listeners.containsKey(key)) {
					for (KeyListener l : listeners.get(key)) {
						if (!map.containsKey(l))
							map.put(l, new LinkedList<Integer>());
						map.get(l).add(key);
					}
				}
			}
			for (Entry<KeyListener, List<Integer>> entry : map.entrySet()) {
				entry.getKey().handle(entry.getValue());
			}
		}
	}
}