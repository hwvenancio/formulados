package br.pucpr.pos.game.model;

/**
 * Abstracts Model Updating logic from Model state
 * 
 * @author hwvenancio
 * 
 * @param <T>
 */
public interface Updater<T extends Model> {

	void update(T model);
}