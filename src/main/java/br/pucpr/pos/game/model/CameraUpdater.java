package br.pucpr.pos.game.model;

/**
 * Changes camera position to track an object at the bottom of the screen
 * 
 * @author hwvenancio
 * 
 */
public class CameraUpdater implements Updater<Camera> {

	private Model trackingModel;

	public CameraUpdater(Model model) {
		trackingModel = model;
	}

	@Override
	public void update(Camera camera) {
		camera.setX(0);
		camera.setY(trackingModel.getY() - 19);
	}
}