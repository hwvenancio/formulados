package br.pucpr.pos.game.model;

import java.net.URL;
import java.util.List;

import br.pucpr.pos.game.physics.CollisionHelper;
import br.pucpr.pos.game.util.Box;
import br.pucpr.pos.game.util.Pair;
import br.pucpr.pos.game.util.Sprite;

/**
 * Represents a track state (see TrackData)
 * 
 * @author hebert
 * 
 */
public class Track extends Model {

	private static final int[] constantPixels = TrackData.generateLine(19, 40, 0);

	private TrackData data;

	public Track() {
		data = new TrackData();
	}

	public Track(URL resource) {
		data = TrackData.fromUrl(resource);
		setCollisionBox(new Box(0, 0, 80, getLength()));
	}

	public TrackData getData() {
		return data;
	}

	public Sprite generateSprite(int y, int height) {
		List<int[]> pixels = data.getPixels();
		Sprite s = new Sprite(79, height);
		for (int j = 0; j < height; j++) {
			if (y + j < 0 || y + j >= pixels.size())
				s.getPixels()[j] = constantPixels;
			else
				s.getPixels()[j] = pixels.get(y + j);
		}
		return s;
	}

	public int getLength() {
		return data.getPixels().size();
	}

	public int calculateFriction(Box collisionBox) {
		Box trackBox = getTranslatedCollisionBox();
		if (!CollisionHelper.overlaps(trackBox, collisionBox)) {
			return 5;
		} else {
			int pixelCount = 0;
			int frictionSum = 0;
			for (int j = collisionBox.y; j < collisionBox.y + collisionBox.height; j++) {
				for (int i = collisionBox.x; i < collisionBox.x + collisionBox.width; i++) {
					pixelCount++;
					if (isGrass(this, i, j)) {
						frictionSum += 5;
					} else {
						frictionSum += 1;
					}
				}
			}
			return frictionSum / pixelCount;
		}
	}

	private static boolean isGrass(Track track, int x, int y) {
		// check if it's out of bounds
		if (x < track.getX() || x >= track.getX() + 80 || y < track.getY() || y >= track.getY() + track.getLength())
			return true;

		Pair<Integer, Integer> segment = track.getData().getSegments().get(y);
		if (x < segment.o1 || x > segment.o1 + segment.o2)
			return true;
		return false;
	}
}