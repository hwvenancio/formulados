package br.pucpr.pos.game.model;

import br.pucpr.pos.game.util.Box;

/**
 * Represents a car state (position, speed and friction). Should use the updater
 * to change it's position based on speed
 * 
 * @author hwvenancio
 * 
 */
public class Car extends Model {

	private int speed;
	private int friction;

	public Car() {
		speed = 0;
		friction = 1;
		setCollisionBox(new Box(0, 0, 5, 5));
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getFriction() {
		return friction;
	}

	public void setFriction(int friction) {
		this.friction = friction;
	}
}