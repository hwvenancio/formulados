package br.pucpr.pos.game.model;

import java.util.Map.Entry;
import java.util.Set;

import br.pucpr.pos.game.util.Triple;

/**
 * Adds all models queued to addition and removes all models queued to removal.
 * Call all models Updater
 * 
 * @author hwvenancio
 * 
 */
public class WorldUpdater implements Updater<World> {

	public void update(World world) {
		for (Model model : world.getRemovalQueue()) {
			world.removeModel(model);
		}
		for (Triple<Integer, Model, Lifecycle<Model>> toAdd : world.getAdditionQueue()) {
			int layer = toAdd.o1;
			Model model = toAdd.o2;
			Lifecycle<Model> lifecycle = toAdd.o3;
			world.addModel(layer, model, lifecycle);
		}
		world.getAdditionQueue().clear();
		world.getRemovalQueue().clear();
		for (Entry<Integer, Set<Model>> layer : world.getLayers().entrySet()) {
			for (Model childModel : layer.getValue()) {
				childModel.update();
			}
		}
	}
}