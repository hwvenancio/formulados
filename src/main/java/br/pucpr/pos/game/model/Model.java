package br.pucpr.pos.game.model;

import br.pucpr.pos.game.render.Renderer;
import br.pucpr.pos.game.util.Box;
import br.pucpr.pos.game.util.Sprite;

/**
 * Represents a base model state (x, y, collision box, update count). Should use
 * the updater to change it's state and the renderer to display it
 * 
 * @author hwvenancio
 * 
 */
public class Model {

	private int x;
	private int y;
	private int updateCount;
	private Box collisionBox;

	private World world;
	private Updater<Model> updater;
	private Renderer renderer;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Box getCollisionBox() {
		return collisionBox;
	}

	public void setCollisionBox(Box box) {
		this.collisionBox = box;
	}

	public Box getTranslatedCollisionBox() {
		if (collisionBox == null)
			return null;
		return new Box(x + collisionBox.x, y + collisionBox.y, collisionBox.width, collisionBox.height);
	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	public Updater<Model> getUpdater() {
		return updater;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setUpdater(Updater updater) {
		this.updater = updater;
	}

	public Renderer getRenderer() {
		return renderer;
	}

	public void setRenderer(Renderer renderer) {
		this.renderer = renderer;
	}

	public int getUpdateCount() {
		return updateCount;
	}

	public int incrementUpdateCount() {
		return updateCount++;
	}

	public int resetUpdateCount() {
		return updateCount = 0;
	}

	public void update() {
		if (updater != null)
			updater.update(this);
	}

	public void draw(Sprite s, Box viewPort) {
		if (renderer != null)
			renderer.draw(s, viewPort, this);
	}
}