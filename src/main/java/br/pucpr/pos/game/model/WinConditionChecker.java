package br.pucpr.pos.game.model;

import br.pucpr.pos.game.core.Scene;

public class WinConditionChecker implements Updater<Model> {

	private Scene scene;
	private Car playerCar;
	private boolean win;

	public WinConditionChecker(Scene scene, Car playerCar) {
		this.scene = scene;
		this.playerCar = playerCar;
		win = false;
	}

	@Override
	public void update(Model model) {
		if(playerCar.getY() < 0) {
			scene.loop = false;
			win = true;
		}
	}

	public boolean isWin() {
		return win;
	}
}