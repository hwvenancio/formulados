package br.pucpr.pos.game.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import br.pucpr.pos.game.ai.CarAI;
import br.pucpr.pos.game.physics.CollisionHelper;
import br.pucpr.pos.game.physics.OpponentCarCollisionHandler;
import br.pucpr.pos.game.render.SpriteRenderer;
import br.pucpr.pos.game.util.Box;

/**
 * Spawn opponent cars and destroy them when outside of screen
 * 
 * @author hwvenancio
 * 
 */
public class CarSpawnerUpdater implements Updater<CarSpawner> {

	private int timeStep;
	private int scanSpeed;

	private Camera camera;
	private int layer;

	private PhysicsUpdater physics;
	private AIUpdater ai;
	private Car playerCar;
	private Track track;
	private CarUpdater carUpdater;
	private List<SpriteRenderer> renderers;
	private Random randomSelector;

	public CarSpawnerUpdater(PhysicsUpdater physics, AIUpdater ai, Car playerCar, Camera camera, Track track, int layer, int ticksPS, CarUpdater carUpdater,
			List<SpriteRenderer> renderers) {
		this.timeStep = ticksPS;
		this.scanSpeed = 2000;

		this.camera = camera;
		this.layer = layer;

		this.physics = physics;
		this.ai = ai;
		this.playerCar = playerCar;
		this.track = track;
		this.carUpdater = carUpdater;
		this.renderers = renderers;

		randomSelector = new Random(10);
	}

	@Override
	public void update(CarSpawner spawner) {
		if (timeStep * spawner.incrementUpdateCount() >= scanSpeed) {
			spawner.resetUpdateCount();

			Box box = camera.getViewPort();
			Box collisionBox;
			Lifecycle<Car> lifecycle;
			for (Car car : new LinkedList<Car>(spawner.getCars())) {
				collisionBox = car.getTranslatedCollisionBox();
				if (!CollisionHelper.overlaps(box, collisionBox)) {
					lifecycle = spawner.getLifecycle(car);
					spawner.getWorld().queueRemoval(car);
					spawner.remove(car);
				}
			}
			if (spawner.getCars().size() < spawner.getMaxCarsOnScreen()) {
				Car car = new Car();
				lifecycle = new CarSpawnerLifecycle(physics, ai, playerCar, camera, track, carUpdater, renderers, randomSelector);
				spawner.getWorld().queueAddition(layer, car, lifecycle);
				spawner.add(car, lifecycle);
			}
		}
	}

	private static class CarSpawnerLifecycle implements Lifecycle<Car> {

		private PhysicsUpdater physics;
		private AIUpdater ai;
		private Car playerCar;
		private Camera camera;
		private Track track;
		private CarUpdater carUpdater;
		private List<SpriteRenderer> renderers;
		private Random randomSelector;
		private CarAI aiListener;
		private OpponentCarCollisionHandler physicsListener;

		public CarSpawnerLifecycle(PhysicsUpdater physics, AIUpdater ai, Car playerCar, Camera camera, Track track, CarUpdater carUpdater,
				List<SpriteRenderer> renderers, Random randomSelector) {
			this.physics = physics;
			this.ai = ai;
			this.playerCar = playerCar;
			this.camera = camera;
			this.track = track;
			this.carUpdater = carUpdater;
			this.renderers = renderers;
			this.randomSelector = randomSelector;
		}

		@Override
		public void initialize(Car car) {
			car.setUpdater(carUpdater);
			int index = randomSelector.nextInt(renderers.size());
			SpriteRenderer renderer = renderers.get(index);
			car.setRenderer(renderer);
			car.setX(track.getData().getSegments().get(camera.getY()).o1 + 8);
			car.setY(camera.getY() - car.getCollisionBox().height);
			car.setFriction(0);

			aiListener = new CarAI(track, car);
			physicsListener = new OpponentCarCollisionHandler(playerCar);

			ai.addListener(aiListener);
			physics.addListener(car, physicsListener);
		}

		@Override
		public void terminate(Car car) {
			physics.removeListener(car);
			ai.removeListener(aiListener);
		}
	}
}