package br.pucpr.pos.game.model;

/**
 * Abstracts Model initialization and finalization. Model life-cycle handlers
 * should implement this interface
 * 
 * @author hwvenancio
 * 
 * @param <T>
 */
public interface Lifecycle<T extends Model> {
	public static final Lifecycle<Model> NULL_LIFECYCLE = new Lifecycle<Model>() {
		@Override
		public void initialize(Model model) {
			// do nothing
		}

		@Override
		public void terminate(Model model) {
			// do nothing
		}
	};

	void initialize(T model);

	void terminate(T model);
}