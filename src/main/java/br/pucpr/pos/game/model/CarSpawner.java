package br.pucpr.pos.game.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents an opponent car spawner state (cars spawned, max number of cars
 * spawned). Should use the updater to spawn cars and destroy them when not on
 * screen
 * 
 * @author hwvenancio
 * 
 */
public class CarSpawner extends Model {

	private int maxCarsOnScreen;
	private Map<Car, Lifecycle<Car>> cars;

	public CarSpawner() {
		maxCarsOnScreen = 1;
		cars = new LinkedHashMap<Car, Lifecycle<Car>>();
	}

	public int getMaxCarsOnScreen() {
		return maxCarsOnScreen;
	}

	public void setMaxCarsOnScreen(int maxCarsOnScreen) {
		this.maxCarsOnScreen = maxCarsOnScreen;
	}

	public List<Car> getCars() {
		return new ArrayList<Car>(cars.keySet());
	}

	public void add(Car car, Lifecycle<Car> lifecycle) {
		cars.put(car, lifecycle);
	}

	public void remove(Car car) {
		cars.remove(car);
	}

	public Lifecycle<Car> getLifecycle(Car car) {
		return cars.get(car);
	}
}