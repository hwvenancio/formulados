package br.pucpr.pos.game.model;

import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.Color.WHITE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import br.pucpr.pos.game.util.Pair;
import br.pucpr.pos.game.util.Sprite;

/**
 * Holds track data (segments and pixels)
 * 
 * @author hwvenancio
 * 
 */
public class TrackData {

	private List<Pair<Integer, Integer>> segments;
	private List<int[]> pixels;

	public TrackData() {
		segments = new ArrayList<Pair<Integer, Integer>>();
		pixels = new ArrayList<int[]>();
	}

	public void addSegment(int x, int w) {
		segments.add(new Pair<Integer, Integer>(x, w));
		pixels.add(generateLine(x, w, pixels.size()));
	}

	public List<Pair<Integer, Integer>> getSegments() {
		return segments;
	}

	public static int[] generateLine(int x1, int w, int y) {
		int x2 = x1 + w;
		Sprite s = new Sprite(80, 1);
		for (int i = 0; i < 79; i++) {
			if (i < x1 || i > x2) {
				s.setBG(i, 0, GREEN.value());
				s.setCode(i, 0, 0x0020);
			} else if (i < x1 + 2 || i > x2 - 2) {
				if ((y) % 8 < 4)
					s.setBG(i, 0, RED.value());
				else
					s.setBG(i, 0, WHITE.value());
				s.setCode(i, 0, 0x0020);
			} else {
				s.setCode(i, 0, 0x0020);
			}
		}
		return s.getPixels()[0];
	}

	public static TrackData fromUrl(URL resource) {
		TrackData t = new TrackData();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(resource.openStream()));
			try {
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					StringTokenizer tokenizer = new StringTokenizer(inputLine, ";");
					int x = Integer.parseInt(tokenizer.nextToken());
					int w = Integer.parseInt(tokenizer.nextToken());
					int r = Integer.parseInt(tokenizer.nextToken());
					for (int i = 0; i < r; i++)
						t.addSegment(x, w);
				}
			} finally {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return t;
	}

	public List<int[]> getPixels() {
		return pixels;
	}
}