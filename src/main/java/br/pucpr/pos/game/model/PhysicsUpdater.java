package br.pucpr.pos.game.model;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import br.pucpr.pos.game.physics.CollisionHelper;
import br.pucpr.pos.game.physics.PhysicsListener;
import br.pucpr.pos.game.util.Box;

/**
 * Invoke all registered {@link PhysicsListener}'s handle() method when a
 * collision happens
 * 
 * @author hwvenancio
 * 
 */
@SuppressWarnings("rawtypes")
public class PhysicsUpdater implements Updater<Model> {

	private Map<Model, PhysicsListener> collisionHandlers;

	public PhysicsUpdater() {
		// listeners
		collisionHandlers = new LinkedHashMap<Model, PhysicsListener>();
	}

	public void addListener(Model model, PhysicsListener l) {
		collisionHandlers.put(model, l);
	}

	public void removeListener(Model model) {
		collisionHandlers.remove(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void update(Model model) {
		if (collisionHandlers.size() > 1) {
			Iterator<Entry<Model, PhysicsListener>> it1 = collisionHandlers.entrySet().iterator();
			while (it1.hasNext()) {
				Iterator<Entry<Model, PhysicsListener>> it2 = collisionHandlers.entrySet().iterator();
				Entry<Model, PhysicsListener> entry1 = it1.next();
				Model model1 = entry1.getKey();
				PhysicsListener handler = entry1.getValue();
				while (it2.hasNext()) {
					Model model2 = it2.next().getKey();
					if (model1 != model2) {
						Box box1 = model1.getTranslatedCollisionBox();
						Box box2 = model2.getTranslatedCollisionBox();
						if (CollisionHelper.overlaps(box1, box2)) {
							handler.handle(model1, model2);
						}
					}
				}
			}
		}
	}
}