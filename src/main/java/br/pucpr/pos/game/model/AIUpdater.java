package br.pucpr.pos.game.model;

import java.util.LinkedList;
import java.util.List;

import br.pucpr.pos.game.ai.AIListener;

/**
 * Invoke all registered {@link AIListener}'s handle() method
 * 
 * @author hwvenancio
 * 
 */
public class AIUpdater implements Updater<Model> {

	private int scanSpeed;
	private int timeStep;

	private List<AIListener> listeners;

	public AIUpdater(int ticksPS) {
		// listeners
		listeners = new LinkedList<AIListener>();

		// game loop
		timeStep = ticksPS;
		scanSpeed = 100;
	}

	public void addListener(AIListener l) {
		listeners.add(l);
	}

	public void removeListener(AIListener l) {
		listeners.remove(l);
	}

	@Override
	public void update(Model model) {
		if (timeStep * model.incrementUpdateCount() > scanSpeed) {
			model.resetUpdateCount();

			for (AIListener l : listeners) {
				l.handle();
			}
		}
	}
}