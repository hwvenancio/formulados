package br.pucpr.pos.game;

import br.pucpr.pos.game.os.win.Kernel32.CHAR_INFO;
import br.pucpr.pos.game.os.win.Kernel32.COORD;
import br.pucpr.pos.game.os.win.Kernel32.SMALL_RECT;
import br.pucpr.pos.game.os.win.WinCon;

import com.sun.jna.Pointer;

public class TestJNA {
	// public static void main(String[] args) throws InterruptedException {
	// Kernel32 k = Kernel32.INSTANCE;
	// HANDLE buffer1 = k.GetStdHandle(-11);
	// HANDLE buffer2 = k.CreateConsoleScreenBuffer( //
	// GENERIC_WRITE //
	// , 0 //
	// , null //
	// , CONSOLE_TEXTMODE_BUFFER //
	// , null);
	//
	// COORD begin = new COORD();
	// begin.x = 0;
	// begin.y = 0;
	//
	// String txt = "Milk\n";
	// k.WriteConsole(buffer1 //
	// , txt //
	// , new DWORD(txt.length()) //
	// , null //
	// , null);
	//
	// txt = "Melk\n";
	// k.WriteConsole(buffer2 //
	// , txt //
	// , new DWORD(txt.length()) //
	// , null //
	// , null);
	//
	// Thread.sleep(2000);
	//
	// k.SetConsoleActiveScreenBuffer( buffer2 );
	//
	// Thread.sleep(2000);
	//
	// k.SetConsoleActiveScreenBuffer( buffer1 );
	//
	// k.CloseHandle(buffer2);
	// }
	// public static void main(String[] args) throws Exception {
	// Kernel32 k = Kernel32.INSTANCE;
	// HANDLE std = k.GetStdHandle(-11); // -11 quer dizer out, isso quer dizer
	// que se você escrever no .err vai ficar branco
	// k.SetConsoleTextAttribute(std, 0x02); // 0x02 quer dizer verde
	// System.out.println("Sucesso!");
	// k.SetConsoleTextAttribute(std, 0x0C); // 0x0C quer dizer vermelho
	// System.out.println("Erro!");
	// }

	/**
	 * Example from:
	 * http://www.tomshardware.com/forum/65918-13-technique-fast-win32
	 * -console-drawing
	 */
	public static void main(String[] args) {
		WinCon k = WinCon.INSTANCE;
		Pointer hOutput = k.GetStdHandle(WinCon.STD_OUTPUT_HANDLE);

		COORD dwBufferSize = new COORD();
		dwBufferSize.X = 80;
		dwBufferSize.Y = 25;
		COORD dwBufferCoord = new COORD();
		dwBufferCoord.X = 0;
		dwBufferCoord.Y = 0;
		SMALL_RECT rcRegion = new SMALL_RECT();
		rcRegion.Left = 0;
		rcRegion.Top = 0;
		rcRegion.Right = 79;
		rcRegion.Bottom = 24;

		CHAR_INFO[] buffer = CHAR_INFO.createArray(25*80);

		k.ReadConsoleOutput(hOutput, buffer, dwBufferSize, dwBufferCoord, rcRegion);

		buffer[5*80 + 10].uChar.set((byte)'H');
		buffer[5*80 + 10].Attributes = 0x0E;
		buffer[5*80 + 11].uChar.set((byte)'i');
		buffer[5*80 + 11].Attributes = 0x0B;
		buffer[5*80 + 12].uChar.set((byte)'!');
		buffer[5*80 + 12].Attributes = 0x0A;

		k.WriteConsoleOutput(hOutput, buffer, dwBufferSize, dwBufferCoord, rcRegion);
	}
}