package br.pucpr.pos.game;

import java.io.IOException;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import br.pucpr.pos.game.os.win.FileAPI;
import br.pucpr.pos.game.os.win.Kernel32.CHAR_INFO;
import br.pucpr.pos.game.os.win.Kernel32.CONSOLE_CURSOR_INFO;
import br.pucpr.pos.game.os.win.Kernel32.CONSOLE_SCREEN_BUFFER_INFO;
import br.pucpr.pos.game.os.win.Kernel32.COORD;
import br.pucpr.pos.game.os.win.Kernel32.SMALL_RECT;
import br.pucpr.pos.game.os.win.WinCon;

public class TestConsole {

	public static void main(String[] args) throws IOException, InterruptedException {
		Console console = create("Char View", 80, 25);
		console.clear();

		Buffer b = new Buffer(16, 16);

		for (int y = 0; y < 16; ++y) {
			for (int x = 0; x < 16; ++x) {
				char c = (char) (y * 16 + x);
				b.write(x, y, c);
			}
		}

		console.write(19, 1, b);

		Thread.sleep(2000);
		console.close();
	}

	public static Console create(String title, int w, int h) throws java.io.IOException {
		return Win32Console.create(title, w, h);
	}
}

class Win32Console implements Console {
	private final CONSOLE_SCREEN_BUFFER_INFO savedcsbi_ = new CONSOLE_SCREEN_BUFFER_INFO();
	private final Pointer hStdOutput_;
	private final Pointer hStdInput_;
	private int width_;
	private int height_;

	public static Win32Console create(String title, int w, int h) {
		if (WinCon.INSTANCE == null)
			return null;
		else
			return new Win32Console(title, w, h);
	}

	public Win32Console(String title, int w, int h) {
		width_ = w;
		height_ = h;

		if (WinCon.INSTANCE.GetConsoleWindow() == null)
			WinCon.INSTANCE.AllocConsole();
		// hStdOutput_ =
		// WinCon.INSTANCE.GetStdHandle(WinCon.INSTANCE.STD_OUTPUT_HANDLE);
		// hStdInput_ =
		// WinCon.INSTANCE.GetStdHandle(WinCon.INSTANCE.STD_INPUT_HANDLE);
		hStdOutput_ = FileAPI.INSTANCE.CreateFile("CONOUT$", FileAPI.GENERIC_READ | FileAPI.GENERIC_WRITE, FileAPI.FILE_SHARE_WRITE, Pointer.NULL,
				FileAPI.OPEN_EXISTING, 0, Pointer.NULL);
		hStdInput_ = FileAPI.INSTANCE.CreateFile("CONIN$", FileAPI.GENERIC_READ | FileAPI.GENERIC_WRITE, FileAPI.FILE_SHARE_READ, Pointer.NULL,
				FileAPI.OPEN_EXISTING, 0, Pointer.NULL);

		WinCon.INSTANCE.GetConsoleScreenBufferInfo(hStdOutput_, savedcsbi_);
		// WinCon.INSTANCE.SetConsoleCP((short) 437);
		if (title != null)
			WinCon.INSTANCE.SetConsoleTitle(title);
		WinCon.INSTANCE.SetConsoleWindowInfo(hStdOutput_, true, new SMALL_RECT((short) 0, (short) 0, (short) 1, (short) 1));
		WinCon.INSTANCE.SetConsoleScreenBufferSize(hStdOutput_, new COORD((short) w, (short) h));
		WinCon.INSTANCE.SetConsoleWindowInfo(hStdOutput_, true, new SMALL_RECT((short) 0, (short) 0, (short) (h - 1), (short) (w - 1)));

		// Disable Ctrl+C
		IntByReference mode = new IntByReference();
		WinCon.INSTANCE.GetConsoleMode(hStdInput_, mode);
		WinCon.INSTANCE.SetConsoleMode(hStdInput_, mode.getValue() & ~WinCon.ENABLE_PROCESSED_INPUT);
	}

	@Override
	// from au.radsoft.console.Console
	public int getWidth() {
		return width_;
	}

	@Override
	// from au.radsoft.console.Console
	public int getHeight() {
		return height_;
	}

	@Override
	// from au.radsoft.console.Console
	public void clear() {
		COORD pos = new COORD((short) 0, (short) 0);
		IntByReference r = new IntByReference();
		WinCon.INSTANCE.FillConsoleOutputCharacter(hStdOutput_, ' ', width_ * height_, pos, r);
		WinCon.INSTANCE.FillConsoleOutputAttribute(hStdOutput_, convert(Color.WHITE, Color.BLACK), width_ * height_, pos, r);
		WinCon.INSTANCE.SetConsoleCursorPosition(hStdOutput_, pos);
	}

	@Override
	// from au.radsoft.console.Console
	public void showCursor(boolean show) {
		CONSOLE_CURSOR_INFO.ByReference cci = new CONSOLE_CURSOR_INFO.ByReference();
		WinCon.INSTANCE.GetConsoleCursorInfo(hStdOutput_, cci);
		cci.bVisible = show;
		WinCon.INSTANCE.SetConsoleCursorInfo(hStdOutput_, cci);
	}

	@Override
	// from au.radsoft.console.Console
	public void setCursor(int x, int y) {
		COORD pos = new COORD((short) x, (short) y);
		WinCon.INSTANCE.SetConsoleCursorPosition(hStdOutput_, pos);
	}

	@Override
	// from au.radsoft.console.Console
	public void write(int x, int y, Buffer b) {
		CHAR_INFO[] chars = CHAR_INFO.createArray(b.getWidth() * b.getHeight());
		for (int xx = 0; xx < b.getWidth(); ++xx) {
			for (int yy = 0; yy < b.getHeight(); ++yy) {
				final CharInfo cell = b.get(xx, yy);
				int i = xx + yy * b.getWidth();
				// chars[i] = new CHAR_INFO(cell.c, convert(cell.fg, cell.bg));
				chars[i].uChar.set((byte) cell.c);
				chars[i].Attributes = convert(cell.fg, cell.bg);
			}
		}
		WinCon.INSTANCE.WriteConsoleOutputA(hStdOutput_, chars, new COORD((short) b.getWidth(), (short) b.getHeight()), new COORD((short) 0, (short) 0),
				new SMALL_RECT((short) y, (short) x, (short) (y + b.getHeight()), (short) (x + b.getWidth())));
	}

	@Override
	// from au.radsoft.console.Console
	public void read(int x, int y, Buffer b) {
		CHAR_INFO[] chars = CHAR_INFO.createArray(b.getWidth() * b.getHeight());
		WinCon.INSTANCE.ReadConsoleOutputA(hStdOutput_, chars, new COORD((short) b.getWidth(), (short) b.getHeight()), new COORD((short) 0, (short) 0),
				new SMALL_RECT((short) y, (short) x, (short) (y + b.getHeight()), (short) (x + b.getWidth())));
		for (int xx = 0; xx < b.getWidth(); ++xx) {
			for (int yy = 0; yy < b.getHeight(); ++yy) {
				final CharInfo cell = b.get(xx, yy);
				int i = xx + yy * b.getWidth();
				cell.c = (char) chars[i].uChar.AsciiChar;
				cell.fg = convertFg(chars[i].Attributes);
				cell.bg = convertBg(chars[i].Attributes);
			}
		}
	}

	@Override
	// from au.radsoft.console.Console
	public void close() {
		WinCon.INSTANCE.SetConsoleWindowInfo(hStdOutput_, true, new SMALL_RECT((short) 0, (short) 0, (short) 1, (short) 1));
		WinCon.INSTANCE.SetConsoleScreenBufferSize(hStdOutput_, savedcsbi_.dwSize);
		WinCon.INSTANCE.SetConsoleWindowInfo(hStdOutput_, true, savedcsbi_.srWindow);
		showCursor(true);
		clear();
	}

	static short convert(Color fg, Color bg) {
		return (short) ((bg.ordinal() << 4) | fg.ordinal());
	}

	static Color convertFg(short attr) {
		return Color.values()[attr & 0xF];
	}

	static Color convertBg(short attr) {
		return Color.values()[(attr >> 4) & 0xF];
	}
}

interface Console {

	int getWidth();

	int getHeight();

	void clear();

	void showCursor(boolean show);

	void setCursor(int x, int y);

	void write(int x, int y, Buffer b);

	void read(int x, int y, Buffer b);

	void close();
}

class Buffer {
	private final CharInfo[][] data_;

	public Buffer(int w, int h) {
		if (w <= 0 || h <= 0)
			throw new IllegalArgumentException("Dimensions must be greater than zero.");

		data_ = new CharInfo[h][w];

		for (int y = 0; y < getHeight(); ++y) {
			for (int x = 0; x < getWidth(); ++x) {
				data_[y][x] = new CharInfo(' ', Color.WHITE, Color.BLACK);
			}
		}
	}

	public int getWidth() {
		return data_[0].length;
	}

	public int getHeight() {
		return data_.length;
	}

	public CharInfo get(int x, int y) {
		if (isValid(x, y))
			return data_[y][x];
		else
			return null;
	}

	private boolean isValid(int x, int y) {
		return y >= 0 && y < getHeight() && x >= 0 && x < getWidth();
	}

	public void clear() {
		fill(0, 0, getWidth(), getHeight(), ' ', Color.WHITE, Color.BLACK);
	}

	public void fill(int x, int y, int w, int h, char c, Color fg, Color bg) {
		for (int dy = 0; dy < h; ++dy)
			for (int dx = 0; dx < w; ++dx) {
				final CharInfo cell = get(x + dx, y + dy);
				if (cell != null) {
					cell.c = c;
					cell.fg = fg;
					cell.bg = bg;
				}
			}
	}

	public void fill(int x, int y, int w, int h, char c) {
		for (int dy = 0; dy < h; ++dy)
			for (int dx = 0; dx < w; ++dx) {
				final CharInfo cell = get(x + dx, y + dy);
				if (cell != null) {
					cell.c = c;
				}
			}
	}

	public void fill(int x, int y, int w, int h, Color fg, Color bg) {
		for (int dy = 0; dy < h; ++dy)
			for (int dx = 0; dx < w; ++dx) {
				final CharInfo cell = get(x + dx, y + dy);
				if (cell != null) {
					cell.fg = fg;
					cell.bg = bg;
				}
			}
	}

	public void write(int x, int y, char ch) {
		final CharInfo cell = get(x, y);
		if (cell != null) {
			cell.c = ch;
		}
	}

	public void write(int x, int y, char ch, Color fg) {
		final CharInfo cell = get(x, y);
		if (cell != null) {
			cell.c = ch;
			cell.fg = fg;
		}
	}

	public void write(int x, int y, char ch, Color fg, Color bg) {
		final CharInfo cell = get(x, y);
		if (cell != null) {
			cell.c = ch;
			cell.fg = fg;
			cell.bg = bg;
		}
	}

	public void write(int x, int y, String s) {
		for (int i = 0; i < s.length(); ++i) {
			write(x + i, y, s.charAt(i));
		}
	}

	public void write(int x, int y, String s, Color fg) {
		for (int i = 0; i < s.length(); ++i) {
			write(x + i, y, s.charAt(i), fg);
		}
	}

	public void write(int x, int y, String s, Color fg, Color bg) {
		for (int i = 0; i < s.length(); ++i) {
			write(x + i, y, s.charAt(i), fg, bg);
		}
	}

	public void read(int x, int y, Buffer b) {
		for (int xx = 0; xx < b.getWidth(); ++xx) {
			for (int yy = 0; yy < b.getHeight(); ++yy) {
				final CharInfo srccell = get(xx + x, yy + y);
				if (srccell != null) {
					final CharInfo dstcell = b.get(xx, yy);
					if (dstcell != null) {
						dstcell.set(srccell);
					}
				}
			}
		}
	}

	public void write(int x, int y, Buffer b) {
		for (int xx = 0; xx < b.getWidth(); ++xx) {
			for (int yy = 0; yy < b.getHeight(); ++yy) {
				final CharInfo dstcell = get(xx + x, yy + y);
				if (dstcell != null) {
					final CharInfo srccell = b.get(xx, yy);
					if (srccell != null) {
						dstcell.set(srccell);
					}
				}
			}
		}
	}
}

enum Color {
	BLACK, DARK_BLUE, GREEN, TEAL, DARK_RED, PURPLE, BROWN, LIGHT_GRAY, GRAY, BLUE, LEMON, CYAN, RED, MAGENTA, YELLOW, WHITE
}

class CharInfo {
	public CharInfo(char cc, Color fgc, Color bgc) {
		c = cc;
		fg = fgc;
		bg = bgc;
	}

	public CharInfo(CharInfo o) {
		c = o.c;
		fg = o.fg;
		bg = o.bg;
	}

	public void set(CharInfo o) {
		c = o.c;
		fg = o.fg;
		bg = o.bg;
	}

	public boolean equals(CharInfo o) {
		return c == o.c && fg == o.fg && bg == o.bg;
	}

	public boolean equals(Object o) {
		if (o instanceof CharInfo)
			return equals((CharInfo) o);
		else
			return false;

	}

	public char c;
	public Color fg;
	public Color bg;
}

enum CharKey {

	NONE, TAB('\t'), ENTER('\n'), BACK_SPACE, SHIFT, CONTROL, ALT, PRINTSCREEN, PAUSE, CAPS_LOCK, NUM_LOCK, SCROLL_LOCK, ESCAPE, SPACE(' '), PAGE_UP, PAGE_DOWN, END, HOME, LEFT, UP, RIGHT, DOWN, INSERT, DELETE, MINUS(
			'-'), EQUALS('='), LEFT_BRACKET('['), RIGHT_BRACKET(']'), SEMICOLON(';'), QUOTE('\''), COMMA(','), PERIOD('.'), SLASH('/'), BACK_SLASH('\\'), BACK_QUOTE(
			'`'), N1('1'), N2('2'), N3('3'), N4('4'), N5('5'), N6('6'), N7('7'), N8('8'), N9('9'), N0('0'), A('A'), B('B'), C('C'), D('D'), E('E'), F('F'), G(
			'G'), H('H'), I('I'), J('J'), K('K'), L('L'), M('M'), N('N'), O('O'), P('P'), Q('Q'), R('R'), S('S'), T('T'), U('U'), V('V'), W('W'), X('X'), Y('Y'), Z(
			'Z'), WIN, MENU, NUM0('0'), NUM1('1'), NUM2('2'), NUM3('3'), NUM4('4'), NUM5('5'), NUM6('6'), NUM7('7'), NUM8('8'), NUM9('9'), CLEAR, DIVIDE('/'), MULTIPLY(
			'*'), SUBTRACT('-'), ADD('+'), DECIMAL('.'), F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,

	MOUSE_MOVED, MOUSE_BUTTON1, MOUSE_BUTTONR, MOUSE_BUTTON2;

	public final char c;

	CharKey() {
		this.c = '\0';
	}

	CharKey(char c) {
		this.c = c;
	}
}