package br.pucpr.pos.game.render;

import br.pucpr.pos.game.model.Model;
import br.pucpr.pos.game.model.Track;
import br.pucpr.pos.game.util.Box;
import br.pucpr.pos.game.util.Sprite;

public class TrackRenderer implements Renderer {

	@Override
	public void draw(Sprite s, Box viewPort, Model model) {
		Track track = (Track) model;
		s.draw(0, 0, track.generateSprite(viewPort.y, 25));
	}
}