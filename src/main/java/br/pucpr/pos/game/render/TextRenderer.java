package br.pucpr.pos.game.render;

import static org.fusesource.jansi.Ansi.Color.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;

import br.pucpr.pos.game.model.Model;
import br.pucpr.pos.game.util.Box;
import br.pucpr.pos.game.util.Sprite;

public class TextRenderer implements Renderer {

	private Sprite sprite;

	public TextRenderer(String text) {
		List<String> lines = new LinkedList<String>();
		int width = 0;
		try {
			BufferedReader in = new BufferedReader(new StringReader(text));
			try {
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					lines.add(inputLine);
					width = Math.max(width, inputLine.length());
				}
			} finally {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		int height = lines.size();
		sprite = new Sprite(width, height);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				sprite.setBG(x, y, BLACK.value());
				sprite.setFG(x, y, WHITE.value());
				String line = lines.get(y);
				if (x < line.length())
					sprite.setCode(x, y, lines.get(y).charAt(x));
				else
					sprite.setCode(x, y, 0x0020);
			}
		}
	}

	@Override
	public void draw(Sprite s, Box viewPort, Model model) {
		s.draw(model.getX() - viewPort.x, model.getY() - viewPort.y, sprite);
	}
}