package br.pucpr.pos.game.render;

import java.util.Map.Entry;
import java.util.Set;

import br.pucpr.pos.game.model.Model;
import br.pucpr.pos.game.model.World;
import br.pucpr.pos.game.util.Box;
import br.pucpr.pos.game.util.Sprite;

public class WorldRenderer implements Renderer {

	private boolean clear;

	public WorldRenderer() {
		this(false);
	}

	public WorldRenderer(boolean clear) {
		this.clear = clear;
	}

	@Override
	public void draw(Sprite s, Box viewPort, Model model) {
		if (clear) {
			for(int j = 0; j < s.getHeight(); j++) {
				for(int i = 0; i < s.getWidth(); i++) {
					s.getPixels()[j][i] = 0x00000020;
				}
			}
		}
		World world = (World) model;
		for(Entry<Integer, Set<Model>> layer : world.getLayers().entrySet()) {
			for(Model childModel : layer.getValue()) {
				childModel.draw(s, viewPort);
			}
		}
	}
}