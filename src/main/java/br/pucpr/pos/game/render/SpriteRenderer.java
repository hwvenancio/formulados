package br.pucpr.pos.game.render;

import br.pucpr.pos.game.model.Model;
import br.pucpr.pos.game.util.Box;
import br.pucpr.pos.game.util.Sprite;

public class SpriteRenderer implements Renderer {

	private Sprite sprite;

	public SpriteRenderer(Sprite sprite) {
		this.sprite = sprite;
	}

	public void draw(Sprite s, Box viewPort, Model model) {
		s.draw(model.getX() - viewPort.x, model.getY() - viewPort.y, sprite);
	}
}