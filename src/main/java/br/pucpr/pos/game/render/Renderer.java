package br.pucpr.pos.game.render;

import br.pucpr.pos.game.model.Model;
import br.pucpr.pos.game.util.Box;
import br.pucpr.pos.game.util.Sprite;

public interface Renderer {

	void draw(Sprite s, Box viewPort, Model model);
}