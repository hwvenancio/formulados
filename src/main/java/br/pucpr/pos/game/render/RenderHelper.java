package br.pucpr.pos.game.render;

import java.util.HashMap;
import java.util.Map;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Color;

import br.pucpr.pos.game.util.Sprite;
import static org.fusesource.jansi.Ansi.ansi;


public class RenderHelper {

	/**
	 * Translates a sprite into ANSI commands.
	 * @param s
	 * @return
	 */
	public static Ansi translate(Sprite s) {
		Map<Integer, Color> colorMap = new HashMap<Integer, Color>();
		for(Color c : Color.values()) {
			colorMap.put(c.value(), c);
		}
		Ansi ansi = ansi();
		int lastFG = 0;
		int lastBG = 0;
		int width = s.getWidth();
		int height = s.getHeight() -1;
		for(int y = 0; y < height; y++) {
			if(y == height -1)
				width--;
			for(int x = 0; x < width; x++) {
				int bg = s.getBG(x, y);
				int fg = s.getFG(x, y);
				int code = s.getCode(x, y);
				if(bg != lastBG) {
					ansi.bg(colorMap.get(bg));
					lastBG = bg;
				}
				if(fg != lastFG) {
					ansi.fg(colorMap.get(fg));
					lastFG = fg;
				}
				if(code == 0)
					ansi.a(' ');
				else
					ansi.a((char)code);
			}
		}
		return ansi;
	}
}