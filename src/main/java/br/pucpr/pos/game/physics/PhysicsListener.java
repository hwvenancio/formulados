package br.pucpr.pos.game.physics;

import br.pucpr.pos.game.model.Model;

public interface PhysicsListener<T1 extends Model, T2 extends Model> {

	public void handle(T1 model1, T2 model2);
}
