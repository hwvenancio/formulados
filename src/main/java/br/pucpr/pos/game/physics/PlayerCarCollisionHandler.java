package br.pucpr.pos.game.physics;

import br.pucpr.pos.game.model.Car;
import br.pucpr.pos.game.model.Model;

public class PlayerCarCollisionHandler implements PhysicsListener<Car, Model> {

	@Override
	public void handle(Car playerCar, Model model) {
		if(model instanceof Car) {
			playerCar.setSpeed(0);
		}
	}
}