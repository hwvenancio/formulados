package br.pucpr.pos.game.physics;

import br.pucpr.pos.game.model.Car;
import br.pucpr.pos.game.model.Model;

public class OpponentCarCollisionHandler implements PhysicsListener<Car, Model> {

	private Car playerCar;

	public OpponentCarCollisionHandler(Car playerCar) {
		this.playerCar = playerCar;
	}

	@Override
	public void handle(Car model1, Model model2) {
		if (model2 == playerCar) {
			// do nothing
		}
	}
}