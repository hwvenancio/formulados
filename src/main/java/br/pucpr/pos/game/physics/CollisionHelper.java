package br.pucpr.pos.game.physics;

import br.pucpr.pos.game.util.Box;
import br.pucpr.pos.game.util.Sprite;

public final class CollisionHelper {

	private CollisionHelper() {
	}

	public static boolean overlaps(Box box1, Box box2) {
		if (box1 == null || box2 == null)
			return false;

		int b1x1 = box1.x;
		int b1x2 = box1.x + box1.width;
		int b1y1 = box1.y;
		int b1y2 = box1.y + box1.height;

		int b2x1 = box2.x;
		int b2x2 = box2.x + box2.width;
		int b2y1 = box2.y;
		int b2y2 = box2.y + box2.height;

		if (b2x1 > b1x2 || b2x2 < b1x1 || b2y1 > b1y2 || b2y2 < b1y1) {
			return false;
		}
		return true;
	}

	/**
	 * Cuts parts of a sprite that is out of bounds
	 * 
	 * @param b
	 * @param x
	 * @param y
	 * @param s
	 * @return
	 */
	public static Sprite clip(Box b, int x, int y, Sprite s) {
		int w = s.getWidth();
		int h = s.getHeight();

		int x2 = x + w;
		int y2 = y + h;
		int bx2 = b.x + b.width;
		int by2 = b.y + b.height;

		if (x2 < b.x || x > bx2 || y2 < b.y || y > by2) {
			return new Sprite(0, 0);
		}

		int newX1 = Math.max(b.x, x);
		int newX2 = Math.min(bx2, x2);
		int newY1 = Math.max(b.y, y);
		int newY2 = Math.min(by2, y2);

		int newW = newX2 - newX1;
		int newH = newY2 - newY1;

		if (newW == w && newH == h)
			return s;

		Sprite newS = new Sprite(newW, newH);
		for (int j = 0; j < newH; j++) {
			for (int i = 0; i < newW; i++) {
				newS.getPixels()[j][i] = s.getPixels()[j + newY1 - y][i + newX1 - x];
			}
		}
		return newS;
	}

	/**
	 * Cuts part of a box that is out of bounds
	 * @param box1
	 * @param box2
	 * @return
	 */
	public static Box clip(Box box1, Box box2) {
		if (box1 == null || box2 == null)
			return new Box(0, 0, 0, 0);

		int b1x1 = box1.x;
		int b1x2 = box1.x + box1.width;
		int b1y1 = box1.y;
		int b1y2 = box1.y + box1.height;

		int b2x1 = box2.x;
		int b2x2 = box2.x + box2.width;
		int b2y1 = box2.y;
		int b2y2 = box2.y + box2.height;

		int newX1 = Math.max(b1x1, b2x1);
		int newX2 = Math.min(b1x2, b2x2);
		int newY1 = Math.max(b1y1, b2y1);
		int newY2 = Math.min(b1y2, b2y2);

		return new Box(newX1, newY1, newX2 - newX1, newY2 - newY1);
	}
}