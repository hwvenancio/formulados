package br.pucpr.pos.game.physics;

import br.pucpr.pos.game.model.Car;
import br.pucpr.pos.game.model.Model;
import br.pucpr.pos.game.model.Track;

public class TrackCollisionHandler implements PhysicsListener<Track, Model> {

	public TrackCollisionHandler() {
	}

	@Override
	public void handle(Track track, Model model) {
		if (model instanceof Car) {
			((Car)model).setFriction(track.calculateFriction(model.getTranslatedCollisionBox()));
		}
	}
}