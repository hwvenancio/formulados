package br.pucpr.pos.game.core;

/**
 * Factory for Win Scene
 * 
 * @author hwvenancio
 * 
 */
public class WinSceneFactory implements SceneFactory {

	@Override
	public SceneLifecycle createScene() {
		return new WinSceneLifecycle();
	}
}