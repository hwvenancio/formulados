package br.pucpr.pos.game.core;

import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;

import br.pucpr.pos.game.model.Camera;
import br.pucpr.pos.game.model.KeyboardUpdater;
import br.pucpr.pos.game.model.Model;
import br.pucpr.pos.game.model.World;
import br.pucpr.pos.game.model.WorldUpdater;
import br.pucpr.pos.game.render.Renderer;
import br.pucpr.pos.game.render.SpriteRenderer;
import br.pucpr.pos.game.render.WorldRenderer;
import br.pucpr.pos.game.util.Sprite;

/**
 * A Scene containing a congratulations message
 * 
 * @author hwvenancio
 * 
 */
public class WinSceneLifecycle implements SceneLifecycle {

	private int FPS;
	private int ticksPS;

	private World world;
	private Camera camera;
	private Model youWin;
	private Sprite youWinSprite;
	private Model keyboard;
	private KeyboardUpdater keyboardUpdater;
	private StopSceneControls quitControls;

	@Override
	public void initialize(Scene scene) {
		FPS = 50;
		ticksPS = 1000 / FPS;

		world = new World();
		world.setUpdater(new WorldUpdater());
		world.setRenderer(new WorldRenderer(true));
		// create congratulations
		youWinSprite = Sprite.fromUrl(Renderer.class.getResource("YouWin.txt"));
		youWin = world.addModel(HUD_LAYER, new Model());
		youWin.setX(20);
		youWin.setY(5);
		youWin.setRenderer(new SpriteRenderer(youWinSprite));
		// create camera
		camera = world.addModel(HUD_LAYER, new Camera());
		// create keyboard
		keyboardUpdater = new KeyboardUpdater(ticksPS);
		keyboard = world.addModel(HUD_LAYER, new Model());
		keyboard.setUpdater(keyboardUpdater);

		// add controls
		quitControls = new StopSceneControls(scene);
		keyboardUpdater.addKeyListener(NativeKeyEvent.VC_ESCAPE, quitControls);
		keyboardUpdater.addKeyListener(NativeKeyEvent.VC_SPACE, quitControls);
		GlobalScreen.addNativeKeyListener(keyboardUpdater);

		scene.setWorld(world);
		scene.setCamera(camera);
		scene.setFPS(FPS);
	}

	@Override
	public void terminate(Scene scene) {
		GlobalScreen.removeNativeKeyListener(keyboardUpdater);
		scene.setNextScene(SceneName.MENU);
	}
}