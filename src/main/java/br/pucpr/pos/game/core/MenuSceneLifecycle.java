package br.pucpr.pos.game.core;

import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;

import br.pucpr.pos.game.model.Camera;
import br.pucpr.pos.game.model.KeyboardUpdater;
import br.pucpr.pos.game.model.Model;
import br.pucpr.pos.game.model.World;
import br.pucpr.pos.game.model.WorldUpdater;
import br.pucpr.pos.game.render.Renderer;
import br.pucpr.pos.game.render.SpriteRenderer;
import br.pucpr.pos.game.render.TextRenderer;
import br.pucpr.pos.game.render.WorldRenderer;
import br.pucpr.pos.game.util.Sprite;

/**
 * A scene containing a logo, some instructions and handles keyboard input to go
 * to the race or quit the game
 * 
 * @author hwvenancio
 * 
 */
public class MenuSceneLifecycle implements SceneLifecycle {

	private int FPS;
	private int ticksPS;

	private World world;
	private Camera camera;
	private Model keyboard;
	private KeyboardUpdater keyboardUpdater;
	private Sprite logoSprite;
	private Model logo;
	private Model instructions;
	private Model controlInstructions;

	private StopSceneControls quitControls;
	private StopSceneControls raceControls;

	@Override
	public void initialize(Scene scene) {
		FPS = 50;
		ticksPS = 1000 / FPS;

		world = new World();
		world.setUpdater(new WorldUpdater());
		world.setRenderer(new WorldRenderer(true));
		// create logo
		logoSprite = Sprite.fromUrl(Renderer.class.getResource("Logo.txt"));
		logo = world.addModel(HUD_LAYER, new Model());
		logo.setRenderer(new SpriteRenderer(logoSprite));
		logo.setX(13);
		logo.setY(3);
		// create instructions
		instructions = world.addModel(HUD_LAYER, new Model());
		instructions.setX(29);
		instructions.setY(19);
		instructions.setRenderer(new TextRenderer("Press 'SPACE' to race!\n Press 'ESC' to quit."));
		// create controls instructions
		controlInstructions = world.addModel(HUD_LAYER, new Model());
		controlInstructions.setX(44);
		controlInstructions.setY(10);
		controlInstructions.setRenderer(new TextRenderer("+------- Instructions -------+\n" + "| Up/Down : Accelerate/Brake |\n"
				+ "| Left/Right : Steer         |\n" + "+----------------------------+"));
		// create camera
		camera = world.addModel(HUD_LAYER, new Camera());
		// create keyboard
		keyboardUpdater = new KeyboardUpdater(ticksPS);
		keyboard = world.addModel(HUD_LAYER, new Model());
		keyboard.setUpdater(keyboardUpdater);

		// add controls
		quitControls = new StopSceneControls(scene);
		keyboardUpdater.addKeyListener(NativeKeyEvent.VC_ESCAPE, quitControls);
		raceControls = new StopSceneControls(scene);
		keyboardUpdater.addKeyListener(NativeKeyEvent.VC_SPACE, raceControls);
		GlobalScreen.addNativeKeyListener(keyboardUpdater);

		scene.setWorld(world);
		scene.setCamera(camera);
		scene.setFPS(FPS);
	}

	@Override
	public void terminate(Scene scene) {
		GlobalScreen.removeNativeKeyListener(keyboardUpdater);
		if (raceControls.isActivated())
			scene.setNextScene(SceneName.RACE);
		else if (quitControls.isActivated())
			scene.setNextScene(SceneName.CREDITS);
	}
}