package br.pucpr.pos.game.core;

import java.util.List;

/**
 * A generic keyboard listener that stops scene loop. In order to track if scene
 * stopped due to this listener, sets the "activated" flag to true
 * 
 * @author hwvenancio
 * 
 */
public class StopSceneControls implements KeyListener {

	private Scene scene;
	private boolean activated;

	public StopSceneControls(Scene scene) {
		this.scene = scene;
		activated = false;
	}

	@Override
	public void handle(List<Integer> pressedKeys) {
		scene.loop = false;
		activated = true;
	}

	public boolean isActivated() {
		return activated;
	}
}