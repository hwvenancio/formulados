package br.pucpr.pos.game.core;

/**
 * Factory for Race Scene
 * 
 * @author hwvenancio
 * 
 */
public class RaceSceneFactory implements SceneFactory {

	@Override
	public SceneLifecycle createScene() {
		return new RaceSceneLifecycle();
	}
}