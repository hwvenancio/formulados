package br.pucpr.pos.game.core;

import java.util.List;

import org.jnativehook.keyboard.NativeKeyEvent;

import br.pucpr.pos.game.model.Car;

/**
 * Handles keyboard input to control player's car.
 * 
 * @author hwvenancio
 * 
 */
public class CarControls implements KeyListener {

	private Car car;

	public CarControls(Car car) {
		this.car = car;
	}

	@Override
	public void handle(List<Integer> pressedKeys) {
		int newX = car.getX();
		int newSpeed = car.getSpeed();
		for (int key : pressedKeys) {
			switch (key) {
			case NativeKeyEvent.VC_UP:
				newSpeed += 3;
				break;
			case NativeKeyEvent.VC_DOWN:
				newSpeed -= 3;
				break;
			case NativeKeyEvent.VC_LEFT:
				newX--;
				break;
			case NativeKeyEvent.VC_RIGHT:
				newX++;
				break;
			}
		}
		newX = Math.max(0, Math.min(79, newX));
		car.setX(newX);
		newSpeed = Math.max(-20, Math.min(200, newSpeed));
		car.setSpeed(newSpeed);
	}
}