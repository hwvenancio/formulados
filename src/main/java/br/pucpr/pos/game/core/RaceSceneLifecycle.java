package br.pucpr.pos.game.core;

import java.util.Arrays;

import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;

import br.pucpr.pos.game.model.AIUpdater;
import br.pucpr.pos.game.model.Camera;
import br.pucpr.pos.game.model.CameraUpdater;
import br.pucpr.pos.game.model.Car;
import br.pucpr.pos.game.model.CarSpawner;
import br.pucpr.pos.game.model.CarSpawnerUpdater;
import br.pucpr.pos.game.model.CarUpdater;
import br.pucpr.pos.game.model.KeyboardUpdater;
import br.pucpr.pos.game.model.Model;
import br.pucpr.pos.game.model.PhysicsUpdater;
import br.pucpr.pos.game.model.Track;
import br.pucpr.pos.game.model.WinConditionChecker;
import br.pucpr.pos.game.model.World;
import br.pucpr.pos.game.model.WorldUpdater;
import br.pucpr.pos.game.physics.PlayerCarCollisionHandler;
import br.pucpr.pos.game.physics.TrackCollisionHandler;
import br.pucpr.pos.game.render.Renderer;
import br.pucpr.pos.game.render.SpriteRenderer;
import br.pucpr.pos.game.render.KeyboardRenderer;
import br.pucpr.pos.game.render.TrackRenderer;
import br.pucpr.pos.game.render.WorldRenderer;
import br.pucpr.pos.game.util.Sprite;

/**
 * A Scene containing a track, the player's car, a opponent spawner, and some
 * "ai" and "physics" handling. Handles keyboard input to control the player's
 * car or go back to main menu. The win condition is to reach the end of the
 * track (when player's Y position is less than 0)
 * 
 * @author hwvenancio
 * 
 */
public class RaceSceneLifecycle implements SceneLifecycle {

	private World world;
	private Car playerCar;
	private Track track;
	private int FPS;
	private int ticksPS;
	private Model keyboard;
	private KeyboardUpdater keyboardUpdater;
	private CarControls carControls;
	private StopSceneControls quitControls;
	private Camera camera;
	private Sprite redCarSprite;
	private Sprite greenCarSprite;
	private Sprite yellowCarSprite;
	private Sprite magentaCarSprite;
	private Sprite driverXSprite;
	private CarUpdater carUpdater;
	private CarSpawner carSpawner;
	private Model ai;
	private AIUpdater aiUpdater;
	private Model physics;
	private PhysicsUpdater physicsUpdater;
	private Model winCondition;
	private WinConditionChecker winConditionChecker;

	public void initialize(Scene scene) {
		FPS = 50;
		ticksPS = 1000 / FPS;

		world = new World();
		world.setUpdater(new WorldUpdater());
		world.setRenderer(new WorldRenderer());
		// load sprites
		redCarSprite = Sprite.fromUrl(Renderer.class.getResource("RedCar.txt"));
		greenCarSprite = Sprite.fromUrl(Renderer.class.getResource("GreenCar.txt"));
		yellowCarSprite = Sprite.fromUrl(Renderer.class.getResource("YellowCar.txt"));
		magentaCarSprite = Sprite.fromUrl(Renderer.class.getResource("MagentaCar.txt"));
		driverXSprite = Sprite.fromUrl(Renderer.class.getResource("DriverX.txt"));
		// create "physics"
		physics = world.addModel(HUD_LAYER, new Model());
		physicsUpdater = new PhysicsUpdater();
		physics.setUpdater(physicsUpdater);
		// create "ai"
		ai = world.addModel(HUD_LAYER, new Model());
		aiUpdater = new AIUpdater(ticksPS);
		ai.setUpdater(aiUpdater);
		// global car updater
		carUpdater = new CarUpdater(ticksPS);
		// create track
		track = world.addModel(TRACK_LAYER, new Track(Model.class.getResource("track0.csv")));
		track.setRenderer(new TrackRenderer());
		physicsUpdater.addListener(track, new TrackCollisionHandler());
		// create player's car
		playerCar = world.addModel(PLAYER_LAYER, new Car());
		playerCar.setUpdater(carUpdater);
		playerCar.setRenderer(new SpriteRenderer(redCarSprite));
		playerCar.setX(37);
		playerCar.setY(track.getLength() - 4);
		physicsUpdater.addListener(playerCar, new PlayerCarCollisionHandler());
		// create keyboard
		keyboardUpdater = new KeyboardUpdater(ticksPS);
		keyboard = world.addModel(HUD_LAYER, new Model());
		keyboard.setUpdater(keyboardUpdater);
		keyboard.setRenderer(new KeyboardRenderer());
		// create camera
		camera = world.addModel(HUD_LAYER, new Camera());
		camera.setMinY(0);
		camera.setMaxY(track.getLength() - 25);
		camera.setUpdater(new CameraUpdater(playerCar));
		// create opponents
		carSpawner = world.addModel(HUD_LAYER, new CarSpawner());
		carSpawner.setUpdater(new CarSpawnerUpdater(physicsUpdater, aiUpdater, playerCar, camera, track, OPPONENT_LAYER, ticksPS, carUpdater //
				, Arrays.asList(new SpriteRenderer(greenCarSprite), new SpriteRenderer(yellowCarSprite), new SpriteRenderer(magentaCarSprite))));

		// create win condition
		winConditionChecker = new WinConditionChecker(scene, playerCar);
		winCondition = world.addModel(HUD_LAYER, new Model());
		winCondition.setUpdater(winConditionChecker);

		// add controls
		carControls = new CarControls(playerCar);
		keyboardUpdater.addKeyListener(NativeKeyEvent.VC_UP, carControls);
		keyboardUpdater.addKeyListener(NativeKeyEvent.VC_DOWN, carControls);
		keyboardUpdater.addKeyListener(NativeKeyEvent.VC_LEFT, carControls);
		keyboardUpdater.addKeyListener(NativeKeyEvent.VC_RIGHT, carControls);
		quitControls = new StopSceneControls(scene);
		keyboardUpdater.addKeyListener(NativeKeyEvent.VC_ESCAPE, quitControls);
		GlobalScreen.addNativeKeyListener(keyboardUpdater);

		scene.setWorld(world);
		scene.setCamera(camera);
		scene.setFPS(FPS);
	}

	public void terminate(Scene scene) {
		GlobalScreen.removeNativeKeyListener(keyboardUpdater);
		if (winConditionChecker.isWin())
			scene.setNextScene(SceneName.WIN);
		else
			scene.setNextScene(SceneName.MENU);
	}
}