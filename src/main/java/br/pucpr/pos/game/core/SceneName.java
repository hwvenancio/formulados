package br.pucpr.pos.game.core;

/**
 * Enumerator for scene's names
 * 
 * @author hwvenancio
 * 
 */
public enum SceneName {

	MENU
	, RACE
	, WIN
	, LOSE
	, CREDITS
}