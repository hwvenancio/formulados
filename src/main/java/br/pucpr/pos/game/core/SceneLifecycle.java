package br.pucpr.pos.game.core;

/**
 * Abstracts scene initialization and finalization. Scene life-cycles
 * handlers should implement this interface
 * 
 * @author hwvenancio
 * 
 */
public interface SceneLifecycle {

	final int TRACK_LAYER = 1;
	final int PLAYER_LAYER = 2;
	final int OPPONENT_LAYER = 3;
	final int HUD_LAYER = 99;

	void initialize(Scene scene);

	void terminate(Scene scene);
}