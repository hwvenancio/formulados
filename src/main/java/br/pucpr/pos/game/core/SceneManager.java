package br.pucpr.pos.game.core;

import java.util.HashMap;
import java.util.Map;

/**
 * Registers scene factories and handles scene transitions
 * 
 * @author hwvenancio
 * 
 */
public class SceneManager {

	private Map<SceneName, SceneFactory> sceneFactoryMap;
	private SceneName firstScene;

	public SceneManager() {
		sceneFactoryMap = new HashMap<SceneName, SceneFactory>();
	}

	public void addFactory(SceneName name, SceneFactory factory) {
		sceneFactoryMap.put(name, factory);
	}

	public SceneName getFirstScene() {
		return firstScene;
	}

	public void setFirstScene(SceneName name) {
		firstScene = name;
	}

	public boolean hasNext(Scene scene) {
		return sceneFactoryMap.containsKey((scene == null) ? firstScene : scene.getNextScene());
	}

	public SceneLifecycle next(Scene scene) {
		return sceneFactoryMap.get((scene == null) ? firstScene : scene.getNextScene()).createScene();
	}
}