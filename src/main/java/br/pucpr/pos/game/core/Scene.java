package br.pucpr.pos.game.core;

import br.pucpr.pos.game.model.Camera;
import br.pucpr.pos.game.model.World;
import br.pucpr.pos.game.os.Screen;
import br.pucpr.pos.game.util.Sprite;

/**
 * A Scene encloses the loop logic for different scenes. A Scene can be a main
 * menu, the actual game, or a win/lose screen.
 * 
 * @author hwvenancio
 * 
 */
public final class Scene {

	private Screen screen;
	private int ticksPS;
	private World world;
	private Camera camera;

	private boolean running;
	public boolean loop;
	private long startTime;
	private SceneName nextScene;

	public Scene() {
		running = false;
		loop = false;
	}

	public Screen getScreen() {
		return screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		if (!running)
			this.world = world;
	}

	public Camera getCamera() {
		return camera;
	}

	public void setCamera(Camera camera) {
		if (!running)
			this.camera = camera;
	}

	public int getFPS() {
		return ticksPS / 1000;
	}

	public void setFPS(int fps) {
		ticksPS = 1000 / fps;
	}

	public void run() throws InterruptedException {
		running = true;
		try {
			if (world != null && camera != null) {
				loop = true;
				startTime = System.currentTimeMillis();
				while (loop) {
					update();
					draw();
					sleep();
				}
			}
		} finally {
			running = false;
		}
	}

	private void update() {
		world.update();
	}

	private void draw() {
		Sprite s = new Sprite(80, 25);
		world.draw(s, camera.getViewPort());
		screen.draw(s);
	}

	private void sleep() throws InterruptedException {
		long sleepTime = ticksPS - (System.currentTimeMillis() - startTime);

		if (sleepTime > 0)
			Thread.sleep(sleepTime);
		else
			Thread.sleep(10);
	}

	public SceneName getNextScene() {
		return nextScene;
	}

	public void setNextScene(SceneName nextScene) {
		this.nextScene = nextScene;
	}
}