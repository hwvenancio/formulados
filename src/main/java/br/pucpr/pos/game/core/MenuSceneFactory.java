package br.pucpr.pos.game.core;

/**
 * Factory for Menu Scene
 * 
 * @author hwvenancio
 * 
 */
public class MenuSceneFactory implements SceneFactory {

	@Override
	public SceneLifecycle createScene() {
		return new MenuSceneLifecycle();
	}
}