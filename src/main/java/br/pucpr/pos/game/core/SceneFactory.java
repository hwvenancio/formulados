package br.pucpr.pos.game.core;

/**
 * Factory that creates a Scene lifecycle
 * 
 * @author hwvenancio
 * 
 */
public interface SceneFactory {

	/**
	 * Creates a new Scene lifecycle
	 * 
	 * @return
	 */
	SceneLifecycle createScene();
}