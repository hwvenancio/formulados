package br.pucpr.pos.game.core;

import java.util.List;

/**
 * Listens to keyboard events. Controls should implement this interface in order
 * to process keyboard input
 * 
 * @author hwvenancio
 * 
 */
public interface KeyListener {

	/**
	 * Executes keyboard input handling logic
	 * 
	 * @param pressedKeys
	 */
	public void handle(List<Integer> pressedKeys);
}