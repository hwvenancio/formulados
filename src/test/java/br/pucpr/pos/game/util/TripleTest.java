package br.pucpr.pos.game.util;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class TripleTest {

	@Test
	public void tripleInList() {
		List<Triple<Object, Object, Object>> list = new ArrayList<Triple<Object, Object, Object>>();

		list.add(new Triple<Object, Object, Object>("a", 1, 'a'));

		assertThat(list, hasItem(new Triple<Object, Object, Object>("a", 1, 'a')));
	}

	@Test
	public void tripleInSet() {
		Set<Triple<Object, Object, Object>> set = new HashSet<Triple<Object, Object, Object>>();

		set.add(new Triple<Object, Object, Object>("a", 1, 'a')); // string, int, char
		set.add(new Triple<Object, Object, Object>('a', 1, "a")); // char, int, string
		set.add(new Triple<Object, Object, Object>('a', 1, 'a')); // char, int, char
		set.add(new Triple<Object, Object, Object>("a", 1, 'a')); // string, int, char
		set.add(new Triple<Object, Object, Object>('a', 1, 'a')); // char, int char
		set.add(new Triple<Object, Object, Object>('a', 1, "a")); // char, int, string

		assertThat(set.size(), is(3));
	}

	@Test
	public void triplesAreEquals() {
		assertThat(new Triple<Object, Object, Object>("a", 1, 'a'), is(equalTo(new Triple<Object, Object, Object>("a", 1, 'a'))));
	}

	@Test
	public void triplesAreNotEquals() {
		assertThat(new Triple<Object, Object, Object>("a", 1, 'a'), is(not(equalTo(new Triple<Object, Object, Object>("b", 2, 'b')))));
	}
}