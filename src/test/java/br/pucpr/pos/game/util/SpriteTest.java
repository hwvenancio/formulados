package br.pucpr.pos.game.util;


import static org.fusesource.jansi.Ansi.Color.*;
import static org.junit.Assert.*;

import org.junit.Test;

import br.pucpr.pos.game.util.Sprite;

public class SpriteTest {

	@Test
	public void testCode() {
		Sprite s = new Sprite(1,1);
		s.setCode(0, 0, 0x2550);
		assertEquals(BLACK.value(), s.getFG(0, 0));
		assertEquals(BLACK.value(), s.getBG(0, 0));
		assertEquals(0x2550, s.getCode(0, 0));
	}

	@Test
	public void testFGColor() {
		Sprite s = new Sprite(1,1);
		s.setFG(0, 0, RED.value());
		assertEquals(RED.value(), s.getFG(0, 0));
		assertEquals(BLACK.value(), s.getBG(0, 0));
		assertEquals(0, s.getCode(0, 0));
	}

	@Test
	public void testBGColor() {
		Sprite s = new Sprite(1,1);
		s.setBG(0, 0, RED.value());
		assertEquals(BLACK.value(), s.getFG(0, 0));
		assertEquals(RED.value(), s.getBG(0, 0));
		assertEquals(0, s.getCode(0, 0));
	}
}