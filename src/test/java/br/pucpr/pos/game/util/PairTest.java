package br.pucpr.pos.game.util;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class PairTest {

	@Test
	public void pairInList() {
		List<Pair<Object, Object>> pairs = new ArrayList<Pair<Object, Object>>();

		pairs.add(new Pair<Object, Object>("a", 1));

		assertThat(pairs, hasItem(new Pair<Object, Object>("a", 1)));
	}

	@Test
	public void pairInSet() {
		Set<Pair<Object, Object>> pairs = new HashSet<Pair<Object, Object>>();

		pairs.add(new Pair<Object, Object>("a", 1));
		pairs.add(new Pair<Object, Object>("b", 2));
		pairs.add(new Pair<Object, Object>("a", 1));
		pairs.add(new Pair<Object, Object>("b", 2));

		assertThat(pairs.size(), is(2));
	}

	@Test
	public void pairsAreEquals() {
		assertThat(new Pair<Object, Object>("a", 1), is(equalTo(new Pair<Object, Object>("a", 1))));
	}

	@Test
	public void pairsAreNotEquals() {
		assertThat(new Pair<Object, Object>("a", 1), is(not(equalTo(new Pair<Object, Object>("b", 1)))));
	}
}