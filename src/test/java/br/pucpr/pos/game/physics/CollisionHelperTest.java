package br.pucpr.pos.game.physics;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import br.pucpr.pos.game.physics.CollisionHelper;
import br.pucpr.pos.game.util.Box;
import br.pucpr.pos.game.util.Sprite;

public class CollisionHelperTest {

	@Test
	public void clipSameSize() {
		Box b = new Box(1, 2, 10, 10);
		Sprite s = new Sprite(10, 10);
		Sprite clipped = CollisionHelper.clip(b, 1, 2, s);

		Sprite expected = s;
		assertArrayEquals(expected.getPixels(), clipped.getPixels());
	}

	@Test
	public void clipSmallerInside() {
		Box b = new Box(1, 2, 10, 10);
		Sprite s = new Sprite(5, 5);
		Sprite clipped = CollisionHelper.clip(b, 2, 3, s);

		Sprite expected = s;
		assertArrayEquals(expected.getPixels(), clipped.getPixels());
	}

	@Test
	public void clipHalfwayThoughAndCheckContent() {
		Box b = new Box(1, 2, 10, 10);
		Sprite s = new Sprite(5, 5);
		s.setCode(0, 0, 1);
		s.setCode(1, 1, 2);
		s.setCode(2, 2, 3);
		s.setCode(3, 3, 4);
		s.setCode(4, 4, 5);
		Sprite clipped = CollisionHelper.clip(b, 0, 0, s);

		Sprite expected = new Sprite(4, 3);
		expected.setCode(1, 0, 3);
		expected.setCode(2, 1, 4);
		expected.setCode(3, 2, 5);
		assertArrayEquals(expected.getPixels(), clipped.getPixels());
	}

	@Test
	public void clipOutOfBounds() {
		Box b = new Box(1, 2, 10, 10);
		Sprite s = new Sprite(5, 5);
		Sprite clipped = CollisionHelper.clip(b, 20, 3, s);

		Sprite expected = new Sprite(0, 0);
		assertArrayEquals(expected.getPixels(), clipped.getPixels());
	}

	@Test
	public void clipLeftLimit() {
		Box b = new Box(1, 1, 2, 2);
		Sprite s = new Sprite(2, 2);
		Sprite clipped = CollisionHelper.clip(b, 0, 1, s);
		
		Sprite expected = new Sprite(1, 2);
		assertArrayEquals(expected.getPixels(), clipped.getPixels());
	}

	@Test
	public void clipRightLimit() {
		Box b = new Box(1, 1, 2, 2);
		Sprite s = new Sprite(2, 2);
		Sprite clipped = CollisionHelper.clip(b, 2, 1, s);
		
		Sprite expected = new Sprite(1, 2);
		assertArrayEquals(expected.getPixels(), clipped.getPixels());
	}

	@Test
	public void clipTopLimit() {
		Box b = new Box(1, 1, 2, 2);
		Sprite s = new Sprite(2, 2);
		Sprite clipped = CollisionHelper.clip(b, 1, 0, s);
		
		Sprite expected = new Sprite(2, 1);
		assertArrayEquals(expected.getPixels(), clipped.getPixels());
	}

	@Test
	public void clipBottomLimit() {
		Box b = new Box(1, 1, 2, 2);
		Sprite s = new Sprite(2, 2);
		Sprite clipped = CollisionHelper.clip(b, 1, 2, s);
		
		Sprite expected = new Sprite(2, 1);
		assertArrayEquals(expected.getPixels(), clipped.getPixels());
	}
}